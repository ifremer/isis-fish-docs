/*
 * #%L
 * Isis-Fish
 * 
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2022 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

function hlJava() {
    $('.language-java').each(function(i, block) {
        hljs.highlightBlock(block);
    });
}

function editSourceGitlab() {
    var path = window.location.pathname;
    if (path.endsWith('/')) {
        path += "index.html";
    }

    var src = path.replace(".html", ".md");
    $("#breadcrumbs .xright").append("| <a href='https://gitlab.nuiton.org/ifremer/isis-fish-docs/-/edit/master/src/site/markdown" + src + "' class='externalLink'>Edit</a>");
}

function insertVideoTag() {
    /*$("h2 a[name='Film_ISIS-Fish'").parent().after(
          "<video poster='http://data.isis-fish.org/isis-fish/movie/ifremerv7.png' width='750' height='422' controls='controls' preload='preload'>" +
          "  <source src='http://data.isis-fish.org/isis-fish/movie/ifremerv7.mp4' type='video/mp4'' />" +
          "  <source src='http://data.isis-fish.org/isis-fish/movie/ifremerv7.webm' type='video/webm' />" +
          "  <source src='http://data.isis-fish.org/isis-fish/movie/ifremerv7.ogv' type='video/ogg'' />" +
          "  Your browser does not support the video tag." +
          "</video>");*/
    $("h2 a[name='ISIS-Fish_video'").parent().after(
          "<video poster='http://data.isis-fish.org/isis-fish/movie/ifremerengv2.png' width='750' height='422' controls='controls' preload='preload'>" +
          "  <source src='http://data.isis-fish.org/isis-fish/movie/ifremerengv2.mp4' type='video/mp4'' />" +
          "  <source src='http://data.isis-fish.org/isis-fish/movie/ifremerengv2.webm' type='video/webm' />" +
          "  <source src='http://data.isis-fish.org/isis-fish/movie/ifremerengv2.ogv' type='video/ogg'' />" +
          "  Your browser does not support the video tag." +
          "</video>");
}

$(function () {
    insertVideoTag();
    hlJava();
    editSourceGitlab();
});
