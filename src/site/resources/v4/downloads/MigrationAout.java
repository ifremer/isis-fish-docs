/*
 * #%L
 * IsisFish
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
 if (group.getId() == 1 && "SudGolfe".equals(arrivalZone.getName()) ) return 1;
	else if(group.getId() ==3 && "SudGolfe".equals(departureZone.getName()) && "Recru".equals(arrivalZone.getName()) ) return 1;
    else if((group.getId() == 16 || group.getId() == 17) && "MigHiver".equals(arrivalZone.getName()) ){

	  	if("gironde".equals(departureZone.getName()) ||
			 "Rochebonne".equals(departureZone.getName()) || 
			 "LandesCote".equals(departureZone.getName())  ||
			 "LandesLarge".equals(departureZone.getName()) ){
			   MatrixND Ngr = N.getSubMatrix(0,group,1) ;
    		   double tot = Ngr.sumAll() ;
		if(tot != 0){
    	double coeff = (0.65*tot - N.getValue(group,arrivalZone)) / (tot-N.getValue(group,arrivalZone));
    	return coeff ;
		}else {return 0;}
        }
      }
return 0;
