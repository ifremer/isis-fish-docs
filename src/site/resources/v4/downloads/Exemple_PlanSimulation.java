/*
 * #%L
 * IsisFish
 * 
 * $Id: Exemple_PlanSimulation.java 322 2017-03-17 14:54:11Z echatellier $
 * $HeadURL: https://svn.codelutin.com/isis-fish-docs/trunk/src/site/resources/v4/downloads/Exemple_PlanSimulation.java $
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package simulationplans;

import static org.nuiton.i18n.I18n.t;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.nuiton.math.matrix.*;
import org.nuiton.topia.*;
import org.nuiton.util.StringUtil;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.GearDAO;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationDAO;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.rule.RuleHelper;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.simulator.SimulationPlanContext;

/**
 * Le principe est d'avoir dans un repertoire un fichier matrix.txt et un 
 * fichier par parametre. Si le parametre s'appelle tac, le fichier sera tac.txt
 * 
 * Les fichiers pour les parametres de type double sont de la forme
 * <pre>
 * -1=0.8
 * 1=1.2
 * </pre> 
 * 
 * Les fichiers pour les parametres de type String sont de la forme
 * <pre>
 * -1=L'equation ecrite sur une seul ligne
 * 1=une autre equation toujours sur une ligne
 * </pre> 
 * 
 * Les fichiers pour les parametres de type Rule sont de la forme
 * <pre>
-1=Cantonnement
rule.-1.parameter.gear=fr.ifremer.isisfish.entities.Gear\#11690286646709\#0.5814158398678262 
rule.-1.parameter.zone=fr.ifremer.isisfish.entities.Zone\#11690286645767\#0.37798185123822536
rule.-1.parameter.beginDate=0                                                                
rule.-1.parameter.endDate=119
rule.-1.parameter.enginSelectivite=false
rule.-1.parameter.beginMonth=8          
rule.-1.parameter.endMonth=11
1=Cantonnement
rule.1.parameter.gear=fr.ifremer.isisfish.entities.Gear\#11690286646709\#0.5814158398678262 
rule.1.parameter.zone=fr.ifremer.isisfish.entities.Zone\#11690286645767\#0.37798185123822536
rule.1.parameter.beginDate=0                                                                
rule.1.parameter.endDate=119
rule.1.parameter.enginSelectivite=false
rule.1.parameter.beginMonth=3          
rule.1.parameter.endMonth=11
 * </pre> 
 */
public class Exemple_PlanSimulation implements SimulationPlan {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(Example_PlanSimulation.class);

    /// On rentre ici le nom des fichiers é lire c'est é dire ceux contenant la matrice d'expérience et 
    /// pour chaque paramétre les valeurs du paramétres correspondant é chaque modalité
    /// Les paramétres é modifier sont ici les bornes des classes de longueur (growth),  la capturabilité, la selectivité et les paramétres de l'AMP.

    static private final String MATRIX = "matrix"; /// Le fichier est donc matrix.txt
    static private final String VBGF = "growth";
    static private final String SELECTIVITY = "selectivity";
    static private final String CATCHABILITY = "capturabilite";
    static private final String AMP = "amp";

    /// Ici figure les paramétres du plan qui apparaitront dans l'interface de lancement de simulation et 
    /// qu on peut éventuellement modifier avant de lancer le plan :

    public int param_parameterNumber = 4; /// ce paramétre n'est normalement pas modifié
    public int param_first = 0; /// on utilise ce paramétre pour indiquer é quelle ligne 
    /// de la matrice on doit commencer é faire tourner les simulations au cas oé on ait dé interompre le plan
    public int param_simulationNumber = 16;/// ce paramétre n'est normalement pas modifié 
    public String param_directory = "Exemple_directory/"; /// Il s'agit du chemin vers le dossier 
    ///contenant les fichier de matrice et paramétres, par défaut le repertoire du go.bat 
    /// donc dans ce cas c'est le dossier Exemple_repertory qui est dans le dossier contenant le go.bat

    /// Déclaration de la matrice d'expérience
    private MatrixND matrix = null;

    /// Non utilisé pour ce script mais é ne pas effacer
    public String[] necessaryResult = {
    // put here all necessary result for this rule
    // example: 
    // ResultName.MATRIX_BIOMASS,
    // ResultName.MATRIX_NET_VALUE_OF_LANDINGS_PER_STRATEGY_MET,
    };

    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur le plan.
     * @return L'aide ou la description du plan
     */
    public String getDescription() throws Exception {
        return t("réalise les expériences d'un plan complet pour 4 parametres é 2 modalités");
    }

    /**
     * Appelé au démarrage de la simulation, cette méthode permet d'initialiser
     * des valeurs
     * @param context La simulation pour lequel on utilise ce plan
     */
    public void init(SimulationPlanContext context) throws Exception {
        /// Création de la matrice d'expérience et chargement :
        File dir = new File(param_directory);
        matrix = MatrixFactory.getInstance().create(
                new int[] { param_simulationNumber, param_parameterNumber });
        matrix.importCSV(new FileReader(new File(dir, MATRIX + ".txt")),
                new int[] { 0, 0 });
        matrix.setSemantic(
                1,
                Arrays.asList(new String[] { VBGF, CATCHABILITY, SELECTIVITY,
                        AMP }));
        System.out.println(matrix);
        System.out.println("amp 0 : " + matrix.getValue(0, AMP));
        /// ajout des regles qui sont modifiées par le plan 
        context.getParam().addExtraRules("Cantonnement");
    }

    /// Création des méthodes qui réccupére dans la matrice la modalité du paramétre pour l'expérience en cours 
    /// Les arguments des méthodes sont le nom du fichier et le numéro de la simulation
    /// (On détermine l'expérience en cours et donc la ligne de la matrice en sommant le numéro de la simulation et le param_first)
    /// pour un double     
    /**
     * @param name le nom de l'element a recuperer
     * @param simulation le numero de la simulation
     * @return
     */
    private double getDouble(String name, int simulation) throws Exception {
        File dir = new File(param_directory);
        Properties prop = new Properties();
        prop.load(new BufferedReader(new FileReader(
                new File(dir, name + ".txt"))));
        int ligne = simulation + param_first;
        int mod = (int) matrix.getValue(ligne, name);
        System.out.println("mod et ligne : " + mod + " " + ligne);
        double result = Double.parseDouble(prop.getProperty("" + mod));
        System.out.println("result : " + result);
        return result;
    }

    /// pour une liste de doubles
    /**
    * @param name le nom de l'element a recuperer
    * @param simulation le numero de la simulation
    * @return
    */
    private double[] getList(String name, int simulation) throws Exception {
        File dir = new File(param_directory);
        Properties prop = new Properties();
        prop.load(new BufferedReader(new FileReader(
                new File(dir, name + ".txt"))));
        int ligne = simulation + param_first;
        int mod = (int) matrix.getValue(ligne, name);
        double[] result = StringUtil.toArrayDouble(prop.getProperty("" + mod)
                .split(";")); // ""+ ca construit une chaine de caractere
        return result;
    }

    /// pour une chaine de characteres (equation)
    /**
     * @param name le nom de l'element a recuperer
     * @param simulation le numero de la simulation
     * @return
     */
    private String getString(String name, int simulation) throws Exception {
        File dir = new File(param_directory);
        Properties prop = new Properties();
        prop.load(new BufferedReader(new FileReader(
                new File(dir, name + ".txt"))));
        int ligne = simulation + param_first;
        int mod = (int) matrix.getValue(ligne, name);
        String result = prop.getProperty("" + mod);
        return result;
    }

    /// pour les paramétres d'une régle de gestion
    /**
    * @param name le nom de l'element a recuperer
    * @param simulation le numero de la simulation
    * @return
    */
    private Rule getRule(TopiaContext topiaContext, String name,
            int simulation) throws Exception {
        File dir = new File(param_directory);
        Properties prop = new Properties();
        prop.load(new BufferedReader(new FileReader(
                new File(dir, name + ".txt"))));
        int ligne = simulation + param_first;
        int mod = (int) matrix.getValue(ligne, name);
        String ruleName = prop.getProperty("" + mod);
        RuleStorage ruleStorage = RuleStorage.getRule(ruleName);
        Rule rule = ruleStorage.getNewRuleInstance();
        RuleHelper.populateRule(mod, topiaContext, rule, prop);
        return rule;
    }

    /**
     * Call before each simulation.
     * 
     * @param context plan context
     * @param nextSimulation storage used for next simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    public boolean beforeSimulation(SimulationPlanContext context,
            SimulationStorage nextSimulation) throws Exception {

        int simNum = nextSimulation.getParameter().getSimulationPlanNumber() + param_first;

        if (simNum < param_simulationNumber + 1) { /// On vérifie qu'il reste des expériences é faire

            /// On utilise les méthodes crées pour récupérer les valeurs des modalités pour l'expérience en cours
            double[] croissance = getList(VBGF, simNum);
            double catchability = getDouble(CATCHABILITY, simNum);
            String selectivity = getString(SELECTIVITY, simNum);
            Rule amp = getRule(nextSimulation.getStorage(), AMP, simNum);

            /// On peut afficher des informations dans les logs
            nextSimulation.getInformation().addInformation(
                    "Growth (" + nextSimulation.getParameter().getSimulationPlanNumber() + ")= " + croissance);
            nextSimulation.getInformation()
                    .addInformation(
                            "Selectivity (" + nextSimulation.getParameter().getSimulationPlanNumber() + ")= "
                                    + selectivity);

            /// Il faut maintenant modifier la valeur des paramétres dans la base de données et les paramétres des régle de la simulation
            /// modif les parametres des regles de gestion :

            // on enleve la regle de la simulation passé (elle est stoquée sous le nom LastAMP) 
            List<Rule> paramRules = nextSimulation.getParameter().getRules();
            //paramRules.remove(context.getValue("lastAMP")); pas forcement utile
            // on nomme la regle que l'on va ajouter LastAMP (pour pouvoir la retrouver a la simulation suivante)
            //context.setValue("lastAMP", amp);
            // on ajoute la regle é la simulation
            paramRules.add(amp);

            /// modif les parametres dans la base de données :
            TopiaContext tx = nextSimulation.getStorage().beginTransaction();
            /// On récupére les objets ISIS a modifier dans la base de données
            PopulationDAO popDAO = IsisFishDAOHelper.getPopulationDAO(tx);
            Population pop = popDAO.findByName("test population");
            List<PopulationGroup> groups = pop.getPopulationGroup();
            GearDAO gearDAO = IsisFishDAOHelper.getGearDAO(tx);
            List<Gear> gears = gearDAO.findAll();

            ///modif borne des classes de taille
            /// On remplace les bornes par les éléments de la liste
            for (PopulationGroup group : groups) {
                group.setMinLength(croissance[group.getId()]);
                group.setMaxLength(croissance[group.getId() + 1]);
            }

            /// modif la capturabilité
            /// On modifie la valeur de la base en la multipliant par la valeur contenue dans la modalité
            MatrixND c = pop.getCapturability();
            for (MatrixIterator i = c.iterator(); i.hasNext();) {
                i.next();
                i.setValue(i.getValue() * catchability);
            }

            /// modif selectivité
            /// On remplace l'equation existante par une autre equation
            for (Gear gear : gears) {
                Selectivity sel = gear.getPopulationSelectivity(pop);
                Equation eq = sel.getEquation();
                eq.setContent(selectivity);
            }

            tx.commitTransaction();
            return true;
        } else {
            return false;
        }

    }

    /**
     * Call after each simulation.
     * 
     * @param context plan context
     * @param nextSimulation storage used for next simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    public boolean afterSimulation(SimulationPlanContext context,
            SimulationStorage lastSimulation) throws Exception {
        return true;
    }

}
