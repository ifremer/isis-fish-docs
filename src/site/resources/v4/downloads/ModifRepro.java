/// exemple 2 mois de repro 
/// un an sur 2 le recrutement a lieu ds les zones repro
int y = context.getSimulationControl().getStep().getYear();
if(y%2 == 1){
// reinitialise result
MatrixHelper.fill(result,0);

double nbM = recruitmentInputs.size(); // nb de mois de repro
double res = 0;
PopulationGroup g0  = pop.getPopulationGroup().get(0); // recrutement arrive ds la classe 0

for(int m=0; m<nbM; m++){
    Double p = recruitmentInputs.get(m).getRecuitementContribution();
    if(p !=null){
        MatrixND oeufsm = recruitmentInputs.get(m).getRepro().copy(); // copy sinon on modifie la matrice de base
		oeufsm = oeufsm.mults(p); // [zoneRepro]
       
        for(MatrixIterator it = oeufsm.iterator(); it.hasNext();) {
        it.next();
            result.setValue(g0,it.getSemanticsCoordinates()[0], it.getValue()+result.getValue(g0,it.getSemanticsCoordinates()[0])); // ajoute au contenu de result pour ne pas ecraser la contri du mois precedent
        }
    }
}
return 0;
}else return 0;
