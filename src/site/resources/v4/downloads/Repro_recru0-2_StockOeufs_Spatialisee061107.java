/*
 * #%L
 * IsisFish
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
//Equation de repro du modele en longueur v3//
//////////////////////////////////////////
/*hypothese : il existe une taille minimum et un declencheur (temp etc) 
la dur�e de repro depend de la taille en avril
Le declencheur a lieu en avril sauf pour rochebonne en mai*/

double ind = 1;
double tot = 0.0;
//boucle pour implementer le recrutement observ�
if (context.getSimulationControl().getDate().getYear() < 3){
double coeffR = 0;
double recru = 0;
		for (Zone zr : zoneRepro) {
		double R = 0 ;
			if ("gironde".equals(zr.getName())){
 				if (Month.APRIL.equals(month)) { coeffR = 0.111;
         		}else if (Month.MAY.equals(month)) { coeffR = 0.272 ;
         		}else if (Month.JUNE.equals(month)) { coeffR = 0.317;
         		}else if (Month.JULY.equals(month)) { coeffR = 0.226 ;
         		}else if (Month.AUGUST.equals(month)) { coeffR = 0.0745;
         		}
				if  (context.getSimulationControl().getDate().getYear() == 0){
				recru = 2.405172e14;
				}else if (context.getSimulationControl().getDate().getYear() == 1){
				recru = 2.04885e14;
				}else if (context.getSimulationControl().getDate().getYear() == 2){
				recru = 3.860327e+14;
			    }else if (context.getSimulationControl().getDate().getYear() == 3){	
				recru = 4.69e+13;
				}else if (context.getSimulationControl().getDate().getYear() == 4){
				recru = 3.196e+14;
			}
			R = recru*coeffR ;
		}
result.setValue(zr,R);
		}
}else{
for(Zone zr : zoneRepro){
	String nomZone = zr.getName();
	tot = 0;
	for (PopulationGroup cr : groups){
	ind = 1;

		if (Month.APRIL.equals(month) && "Rochebonne".equals(nomZone)) {
		ind = 0;
		}else if (Month.JUNE.equals(month)&&!"Rochebonne".equals(nomZone)&&cr.getId() < 13){ //hyp orange A
		//}else if (Month.JUNE.equals(month)&&!"Rochebonne".equals(nomZone)&&cr.getId() < 14){//hyp orange B
		ind = 0;

		}else if (Month.JULY.equals(month)){
			if (!"Rochebonne".equals(nomZone)){
				if (cr.getId() < 15) ind =0;
			}else {
				if (cr.getId() <13 ) ind = 0 ; //hyp orange A
				//if (cr.getId() <14 ) ind = 0 ; //hyp orange B
			}
		}else if (Month.AUGUST.equals(month)){
			if (!"Rochebonne".equals(nomZone)){
				ind =0;
			}else {
				if (cr.getId() <15 ) ind = 0 ;
			}
		}
double fec = cr.getReproductionRate();
//if(context.getSimulationControl().getDate().getYear() == 5 || context.getSimulationControl().getDate().getYear() == 6){		
//fec = fec*0.8 ;
//}

tot += fec*cr.getMeanWeight()*N.getValue(cr,zr)/2.0*ind;
	}// fin de la boucle sur les classes
   
	result.setValue(zr,tot);
	
} // end of boucle for (areas)
}// end of condition on year
return 0;
