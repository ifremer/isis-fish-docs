package rules;

/*
 * #%L
 * IsisFish
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.t;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import scripts.SiMatrix;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.Date;
import fr.ifremer.isisfish.util.Doc;

/**
 * EffortReduction.java
 *
 * Created: 3 septembre 2008
 *
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision: 204 $
 */
public class EffortReduction extends AbstractRule {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(EffortReduction.class);

    @Doc("Begin date")
    public Date param_beginDate = new Date(0);
    @Doc("End date")
    public Date param_endDate = new Date(59);

    @Doc("Pourcentage de reduction d effort applique.")
    public double param_PercentReduction = 0.5;

    boolean first = true;

    protected String[] necessaryResult = {};

    /**
     * @return the necessaryResult
     */
    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * @return L'aide ou la description de la regle
     */
    @Override
    public String getDescription() {
        return t("Reduce monthly effort of each strategy of the percent indicated");
    }

    /**
     * Appele au demarrage de la simulation, cette methode permet d'initialiser
     * des valeurs.
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void init(SimulationContext context) throws Exception {
    }

    /**
     * La condition qui doit etre vrai pour faire les actions
     * @param context La simulation pour lequel on utilise cette regle
     * @return vrai si on souhaite que les actions soit faites
     */
    @Override
    public boolean condition(SimulationContext context, Date date, Metier metier)
            throws Exception {

        boolean result = true;
        if (date.before(param_beginDate)) {
            result = false;
        } else if (date.after(param_endDate)) {
            result = false;
        }
        if (result)
            System.out.println("condition vraie");
        return result;
    }

    /**
     * Si la condition est vrai alors cette action est executee avant le pas
     * de temps de la simulation.
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void preAction(SimulationContext context, Date date, Metier metier)
            throws Exception {
        // shinte la boucle metier
        if (first) {
            first = false;
            SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
            List<Strategy> strs = siMatrix.getStrategies(date);
            for (Strategy str : strs) {
                System.out.println("strategy evaluee : " + str.getName());

                /* Dans un premiere temps tant que l inactivit� est un entier on utilise 
                * la proportion du nombre de bateaux de la strategie pour reduire l effort
                * ce qu on ferait aussi pour une mesure de reduction du nombre de bateaux mais 
                * comme actuellement on ne tient pas compte de l economie... ca revient au meme
                * En fait passer par l inactivit� n est pas la meilleure facon de modifier 
                * l effort le mieux serait d agir sur un autre coeff qui est multipli� a 
                * l effort (Fstd ou ciblage) car comme ca le code serait generique mais on 
                * ne verrait pas que l effort nominal est modifi�...
                */

                double propOld = str.getProportionSetOfVessels();
                double newProp = propOld * (1 - param_PercentReduction);
                str.setProportionSetOfVessels(newProp);
            }
        }
    }

    /**
     * Si la condition est vrai alors cette action est executee apres le pas
     * de temps de la simulation.
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void postAction(SimulationContext context, Date date, Metier metier)
            throws Exception {
        first = true;
    }

}
