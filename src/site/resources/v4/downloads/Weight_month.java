/*
 * #%L
 * IsisFish
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
//Weight equation 

/*Based on legth-weight key based on PelGas survey 2000-2005
W =  0.004184069 * L ^  3.200812
for Spring and summer and juveniles (group <15)*/

Date date = context.getSimulationControl().getDate();
if(group.getId() == 0){ return 0;
}else if (group.getId() < 15){
	double Poids  =  (0.004184069 * Math.pow((group.getMaxLength() + group.getMinLength())/2 , 3.200812))*0.001;
	return Poids ;

}else if(Month.JULY.equals(date.getMonth()) || Month.AUGUST.equals(date.getMonth())){
	if(group.getId() == 15) return 0.02655; //(Pelgas for group 14)
	else if(group.getId() == 16) return 0.03294; // Pelgas for group 15
	else if(group.getId() == 17) return 0.04268; // Pelgas for group 16

/* based on market sampling in Autumn and winter */
}else if (date.getMonth().after(Month.AUGUST) & date.getMonth().before(Month.MARCH)){
	if(group.getId()==15){ return 0.018;
	}else if(group.getId()== 16){ return 0.031;
	}else if(group.getId()== 17){ return 0.040;
	}else return 0; 	

} else {

double Poids  =  (0.004184069 * Math.pow((group.getMaxLength() + group.getMinLength())/2 , 3.200812))*0.001;
	return Poids ;
}
return 0;
