// exemple eq stock recrutement simple (1 seul mois de repro) R = k * biomass 
//class RecrutementInputMap  recrutementInputs;
double k = 10000; 
double biom = recruitmentInputs.get(0).getBiomass().sumAll(); // zone x group
Double p = recruitmentInputs.get(0).getRecuitementContribution();
double res=0;
PopulationGroup g0  = pop.getPopulationGroup().get(0);
if(p != null) res = k*biom*p; // test necessaire car p est null si le mois de repro demandé ne contribue pas au mois de recru en cours
for(Zone z : pop.getRecruitmentZone() ){
    result.setValue(g0,z,res / pop.getRecruitmentZone().size());
}
return 0;
