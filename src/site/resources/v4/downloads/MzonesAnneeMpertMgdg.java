/*
 * #%L
 * IsisFish
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
//test survies martin
Date date = context.getSimulationControl().getDate();

// aov survie contrast = sum pour month et year / ref = north(5) / Megg=pert Madults = 1.49
/*double [] Gir = { 108.157079907313,104.040441019196,114.867357631927,102.775218050167,95.5102675884572,97.4002336900792,97.6896434553373,95.698434692503 };
 double[] Lc = {  1000,160.131882990035,1000,149.685042379658,119.942294122065,125.499807305511,126.431453048777,120.459876791989 };
 double[] Ll = { 127.063329543669,118.855448855865,145.161762228118,116.576747614374,104.931458169551,107.770091172289,108.214891158855,105.209212529286 };
 double[] Ro = {  103.573871689489,100.116829822123,108.991227355935,99.0360699306787,92.6833694996939,94.3585183099993,94.6137091311044,92.8508073480452 };
 double[] No = {  110.499702421585,106.002939231345,118.004634498226,104.634276240226,96.8752699349442,98.8790216414094,99.1866992785972,97.0743501369063 };
*/
// aov survie contrast = sum pour month et year / ref = north(5) / Megg=gdg Madults = 2.84 
double [] Gir = { 83.2167553797197,80.0493868483888,88.3796863724735,79.0759161296458,73.4862163528401,74.9403684701997,75.1630422115824,73.630993337216 };
double[] Lc = {  1000,123.206504342530,1000,115.168637747853,92.2843752641238,96.5603617783932,97.2771760265618,92.6826067110963 };
double[] Ll = { 97.763345880174,91.4481495012754,111.688396803782,89.6949020582122,80.7349411949353,82.9190037491769,83.2612357297987,80.948647192079 };
double[] Ro = {  79.6904053946439,77.0305350684429,83.8585538106882,76.1989914322092,71.3111827171539,72.6000530240379,72.7963984889233,71.4400104784017 };
double[] No = {  85.0191842626395,81.5593456397455,90.793527442933,80.5062875003543,74.5364579686243,76.0781574647785,76.3148866247337,74.6896315609755 };
 

int y = date.getYear();
if (group == null){ return 0;
} else if (group.getId() ==  0 ){
	if(y < 8){

// script effet annee * zones
			if ("gironde".equals(zone.getName())){return Gir[y];}else if ("Rochebonne".equals(zone.getName())) {return Ro[y];}else if ("LandesCote".equals(zone.getName())){return Lc[y];}else if ("LandesLarge".equals(zone.getName())){return Ll[y];}else if ("MigHiver".equals(zone.getName())){return No[y];}else return 0;

	}else return 102.9699; 
//return 102.9699;
} else if (group.getId() ==  1 ){ return 0 ;
//Mpert 1.49
//} else if (group.getId() ==  2 ){ return 9.13896671274146 ; } else if (group.getId() ==  3 ){ return 6.52011321674013 ; } else if (group.getId() ==  4 ){ return 5.07483463318751 ; } else if (group.getId() ==  5 ){ return 4.15653572014739 ; } else if (group.getId() ==  6 ){ return 3.5208050737082 ; } else if (group.getId() ==  7 ){ return 3.05436780362196 ; } else if (group.getId() ==  8 ){ return 2.69743614913744 ; } else if (group.getId() ==  9 ){ return 2.41544170418373 ; } else if (group.getId() ==  10 ){ return 2.18699518587283 ; } else if (group.getId() ==  11 ){ return 1.99814758538767 ; } else if (group.getId() ==  12 ){ return 1.83941167360153 ; } else if (group.getId() ==  13 ){ return 1.70410904289053 ; } else if (group.getId() ==  14 ){ return 1.58740166718328 ; } else if (group.getId() ==  15 ){ return 1.49 ; } else if (group.getId() ==  16 ){ return 1.39 ; } else if (group.getId() ==  17 ){ return 1.49 ; }else return 0;	

//Mgdg 2.84
} else if (group.getId() ==  2 ){ return 11.6872477676121 ; } else if (group.getId() ==  3 ){ return 8.99134625041905 ; } else if (group.getId() ==  4 ){ return 7.39969983293818 ; } else if (group.getId() ==  5 ){ return 6.33559260446452 ; } else if (group.getId() ==  6 ){ return 5.56804905759846 ; } else if (group.getId() ==  7 ){ return 4.98515455624756 ; } else if (group.getId() ==  8 ){ return 4.52564872475113 ; } else if (group.getId() ==  9 ){ return 4.15299824047493 ; } else if (group.getId() ==  10 ){ return 3.84398003597826 ; } else if (group.getId() ==  11 ){ return 3.5830824617687 ; } else if (group.getId() ==  12 ){ return 3.35952602350689 ; } else if (group.getId() ==  13 ){ return 3.16557278228505 ; } else if (group.getId() ==  14 ){ return 2.99551574007537 ; } else if (group.getId() ==  15 ){ return 2.84 ; } else if (group.getId() ==  16 ){ return 2.74 ; } else if (group.getId() ==  17 ){ return 2.84 ; }else return 0;