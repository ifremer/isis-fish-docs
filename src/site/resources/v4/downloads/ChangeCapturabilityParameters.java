/* 
Script de presimulation permettant de remplacer les parametres des capturabilite au debut de chaque simulation par des valeurs contenues dans un fichier csv.
Le chemin menant au csv tient compte du nom de la simulation en cours.
*/

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import scripts.SiMatrix;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.EffortDescriptionDAO;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.MetierDAO;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.MetierSeasonInfoDAO;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.util.Doc;
import org.nuiton.math.matrix.*;
import java.io.File;
import java.io.FileReader;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.*;
import org.nuiton.util.*;
import org.nuiton.topia.*;
import fr.ifremer.isisfish.*;
import fr.ifremer.isisfish.types.*;
import fr.ifremer.isisfish.entities.*;

// recuperation du nom de la simulation en cours (ex : "simu_i0")
String simu = context.getSimulationControl().getId(); 

System.out.println("nom de la simu en cours : " + simu);

String[] decoupe = simu.split("_") ;

// recuperation de l'identifiant de la simulation ("i0")
String ind = decoupe[1];

// construction du chemin menant au csv avec l'identifiant de la simulation
String path = "/home/user/ISIS-Fish/calibrISIS_1pop/RUN/" + ind + "/q.csv";

File Accessibility = new File(path);

MatrixND matAccessibility = MatrixFactory.getInstance().create(Accessibility);

System.out.println("matrice importee" + matAccessibility); // afficher la matrice contenant les valeurs issues du fichier csv

for (Population pop : context.getSimulationStorage().getParameter().getPopulations()) {
  
  pop = (Population) context.getDB().findByTopiaId(pop.getTopiaId()); // recupere population ciblee
  
  MatrixND c = pop.getCapturability(); // recupere la matrice de capturabilite
  System.out.println("ancienne matrice" + pop + c); // afficher la matrice contenant les anciens parametres de capturabilite
  for (MatrixIterator i = c.iterator(); i.hasNext();){
    i.next() ;
    Object [] sem = i.getSemanticsCoordinates();
    PopulationGroup group = (PopulationGroup)sem[0];
    double q = (double)matAccessibility.getValue(group.getId());
    i.setValue(q);
  }
  System.out.println("nouvelle matrice" + pop + c); // cette nouvelle matrice doit contenir les valeurs de la "matrice importee" 
}    