/*
 * #%L
 * IsisFish
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
// Adult migration coefficients {2000,...,2008,mean}
double [] gir = {0.2523304,0.2086467,0.5008767,0.1155539,0.1063631,0.5764831,0.2469257,0.2556178,0.3414639,0.289};
double [] lc = {0.05400881,0.16173449,0.04129422,0.14009017,0.29670246,0.00000000,0.30205351,0.14407688,0.19059324,0.148};
double [] ll = {0.138540813,0.184829055,0.252067222,0.108165542,0.363177244,0.299495371,0.002653738,0.112834679,0.151165424,0.179};
double [] ro = {0.4937572,0.3623039,0.1978877,0.4360312,0.2149993,0.1233157,0.4476018,0.4413241,0.3155026,0.337};
double [] no = { 0.0613627737,0.0824858520,0.0078741776,0.2001591791,0.0187579836,0.0007059103,0.0007652638,0.0461465156,0.0012747868,0.047};
// hindcast model
int y = context.getSimulationControl().getDate().getYear();
// prediction model : mean
//int y = 9;
if(group.getId() > 14 && "MigHiver".equals(departureZone.getName())){
	if("gironde".equals(arrivalZone.getName()))return gir[y];
	else if("Rochebonne".equals(arrivalZone.getName())) return ro[y];
	else if("LandesCote".equals(arrivalZone.getName())) return lc[y];
	else if("LandesLarge".equals(arrivalZone.getName())) return ll[y];
	}return 0;