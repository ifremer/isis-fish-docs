/*
 * #%L
 * IsisFish
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
Date date = context.getSimulationControl().getDate() ;
Population pop = group.getPopulation() ;
String name = "matrixCatchWeightPerStrategyMetPerZonePop" ;
MatrixND landing = context.getResultManager().getMatrix(date,pop,name).copy();
MatrixND land = landing.getSubMatrix(0,1,4); 
Double landingSum = land.sumAll();

double[] Coef10 = {0,-0.275977,-0.093828,-2.89188,-2.437983,2.528421,0.194449,1.10878,0.529903,0.527172,1.503888,1.410975};
double[] Coef20 = {-0.268437,-0.427446,-0.482732,-1.824395,-1.41204,0.203052,-0.550787,-0.28221,-0.236633,-0.385919,-0.342456,-1.918256};
double[] Coef30 = {-0.808865,-0.804389,-1.133971,-2.13875,-1.91298,-0.643282,-1.324481,-0.641541,-0.543603,-0.830526,-0.486578,-4.372755};
double[] Coef40 = {-1.824897,-0.43577,-1.155683,-3.574861,-2.3858,-0.977611,-0.949024,-0.233802,-0.412412,0.392487,-0.321009,-0.413922}; 
if (group.getMeanWeight() <12/1000){
coeff= Coef40;
}else if(group.getMeanWeight() <20/1000){
coeff= Coef30;
}else if(group.getMeanWeight() <33/1000){
coeff= Coef20;
}else { coeff= Coef40;}
 double prix = -0.356622*Math.log(landingSum)+ coeff[date.getMonth().getMonthNumber()];
return prix;