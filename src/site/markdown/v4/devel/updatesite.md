<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# Update this site

To update this site, make sure to:

* have an account on forge [gitlab.nuiton.org](https://gitlab.nuiton.org/)
* be allowed to commit on projet [isis-fish-docs](https://gitlab.nuiton.org/ifremer/isis-fish-docs)
* click on `Edit` link in top menu

<img src="../../images/devel/site/edit.png" />

* update [Markdown](https://en.wikipedia.org/wiki/Markdown) content
* enter a commit message
* press `Commit changes` button (site will be updated few minutes later)
