<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# General architecture

ISIS-Fish comprises three modules
- Data management and storage
- User interface
- Simulation engine


## Data management and storage

<img src="../../images/devel/isis-architecture-modules.png" />

The application is first described formally using UML (Unified Modeling Language).
A code generator is then used to create all the classes representing the application.
The application methods themselves are not generated but written by inheritance from
the generated classes. The application methods in ISIS-Fish account for about 15% of
the code of the application classes and persistence classes.

<img src="../../images/devel/isis-architecture-legend.png" />
<img src="../../images/devel/isis-architecture-modelisation.png" />

Persistence is implemented using ToPIA (Tools for Portable and Independent
Architecture) which provides an abstraction of the persistence library.
Currently we use [Hibernate](https://hibernate.org/orm/) for the ORM
(Object to Relational Mapping) and the H2 database engine.

Libraries have been written for managing text files such as the scripts.
All data (Region, Simulation, Script) can be synchronized with a server
using the Java SVN (subversion) library ([svnkit](https://svnkit.com/)).

<img src="../../images/devel/isis-architecture-persistence.png" />


## User interface

The user interfaces use the Java Swing library. To simplify creating and maintaining
the interfaces and to create a clean division between the interface code and the
application code the Jaxx library is used with interfaces defined in XML. This has the
advantage that CSS can be used to define the appearance of the interface. Previously,
we used SwiXAT, but Jaxx is compiled into Java files which can be reused in conventional
systems.

The interfaces use a number of graphic components.

Most of the interfaces are not specific to ISIS-Fish and may be easily reused in other
simulation projects. Only the data entry for the fleet model and a small number of tabs
in the simulation control window are specific to ISIS-Fish.

<img src="../../images/devel/isis-architecture-interface.png" />


## Simulation engine

The simulation engine is not specific to ISIS-Fish. All the code specific to ISIS-FISH
is written in Java and can be modified using the script editors in the user interface.

All other components are directly reusable.

<img src="../../images/devel/isis-architecture-simulateur.png" />
<img src="../../images/devel/isis-architecture-simulation.png" />
