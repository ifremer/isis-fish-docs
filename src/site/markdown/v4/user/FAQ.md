<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# FAQ

## What do the version numbers such as 3.1.2.1 or 4.0.0.0 mean?

* The first number is the major version: 3.1.3.1 is ISIS-Fish V3 and 4.0.0.0 is ISIS-Fish V4.
* The second number is database version: V3.1.3.1 uses version 1 of the database for ISIS-Fish V3 and V4.0.0.0 uses
  version 0 of the database for ISIS-Fish V4.
* The third number is the version of the scripts to be used (for a given major version and database version)
* The fourth number is the minor version number.

When a particular ISIS-Fish version has been used, the data can be reused with any other version of ISIS-Fish with the
same major, database and script versions, i.e. where only the minor version is different.

If an ISIS-Fish version with a higher database version is used, then all data will be converted to the higher database
version and cannot then be used with a lower database version.

Two different major versions of ISIS-Fish may be installed at the same time as they do not share configuration files:
they are two completely independent applications.

It is, therefore, possible to have ISIS-Fish V3 and ISIS-Fish V4 executing at the same time.


## What are the limitations on using scripts?

You must never modify an object returned by a method in a script as these are cached. If you modify a cached object,
then the next time you call the method you may get the modified object rather than the expected object. For example, if
a method returns a ``List`` then a method that modifies that ``List`` must first make a copy and then modify the copy.
If the method only reads the content of that ``List``, then no precautions are required.

## Scripts

### Where do I find out about ISIS-Fish classes and methods?

* The [UML class diagram](../../images/IsisFishModel.png) gives the ISIS classes, their methods and their interactions.
* The [ISIS-Fish API](http://api.isis-fish.org/) gives a formal definition of the applications program interface.
* Most scripts are in `script/SiMatrix.java` and can be accessed through the ISIS-Fish script editor.
  The sources can be retrieved from the repository at https://gitlab.nuiton.org/ifremer/isis-fish.
* Other scripts  can accessed on line at [ISIS-Fish source code](https://gitlab.nuiton.org/ifremer/isis-fish/tree/master/src/main/java/fr/ifremer/isisfish).


### How do I update my local copies of the scripts?

Scripts stored locally can be synchronized with those on the server using the main File menu: File / Server
synchronization. This compares the scripts stored locally with those on the server and shows which scripts are
different. Check the boxes for the scripts to be synchronized with the server and click OK.

N.B. If there is an error in a script, the synchronization process will modify the script. If the line modified is not
the same as on the server, both lines will be preceded in the file by ``<<<<``. Select the line to be kept and delete or
comment out the other, otherwise the script will not execute.

Copies of all the scripts can be retrieved from the repository at https://gitlab.nuiton.org/ifremer/isis-fish.

### Will my locally written scripts be affected by synchronization?

Locally written scripts are not checked during synchronization.

### How do I update local scripts derived from standard scripts?

If a local script is based on a script on the server, then you must keep a record of the original script. If a script on
the server is modified, then you can find the corresponding local scripts and apply the same modifications.

### How do I add a tooltip for a rule or simulation plan?

Use the script editor to modify the script defining the rule or simulation plan:

* add import `fr.ifremer.isisfish.annotation.Doc` to the imports,
* add an `@Doc` annotation, for example `@Doc("the parameter Zone corresponds to the target zone")` above the
  declaration for the parameter, for example `public Zone param_zone = null;`

### How do I send a message from a script to the log file?

`System.out.println (“hello world”);`

### How do I use the API?

See the API [tutorial](tutorials/useAPI.html).

### How do I set up simulation plan scripts?

See the [simulation plan tutorial](tutorials/simulationPlan.html)

### What is the structure for a rule or simulation plan?

The simulation process is shown schematically at the bottom of the [ISIS-Fish architecture](../devel/architecture.html) page and described on the [simulation plan](usermanual/analysisPlan.html) page. The following document SimulatorRulesTrainofEvents.pdf details the necessary steps, methods and the specific train of events
that should be included in a simulator and a rule.

[Default Simulator Rules Train of processes](./downloads/SimulatorRulesTrainofEvents.pdf).


### Why is one of my rule parameters behaving bizarrely?

Rule parameters are initialized at the start of each simulation and at the start of each step. They should not,
therefore, be modified directly: make copies before modifying the value.

## User interfaces


### How do I check the fishery parameters?

* To check the whole fishery, click the **Check** button at the bottom of the **Region** window.

  - A cyan background in the Region check results window shows parameters that are not set but OK.
  - An orange background in the Region check results window shows parameters that are unset but required or that have
    invalid values.
* To check the syntax of an equation, click the **Check** button in the equation editor.
* To check the syntax of a script, click the **Check** button at the top of the script editor window. The local script
  may be compared against the copy on the server using the **Server/Display diff with server version** menu item.

### What does Tag mean in the Simulation launcher Advanced parameters tab?

The tags are the names of **Free parameters** that are set in the launcher to control exports or the simulator or to modify
equations.

For example the tag ecoResult could be used to simulate with or without economic variables by setting it `true` or `false`.
```
 if (!"false".equalsIgnoreCase(param.getTagValue().get("ecoResult"))) {
   control.setText("Add economics results");
   saveGravityModel(date, resManager, gravityModel);
 }
```

### How is the fishing effort calculated?

The fishing effort standardization formula uses the **Standardization factor** defined for the fishing gear (**Gear**
tab) and the number of **Fishing operations** and the **Gears number per operation** defined for the set of vessels
(**Metier parameters** tab).

`StdEffortPerHour = Fstd * FishingOperationNumber * GearNumberPerOperation.`

The number of **Fishing operations** and **Gears number per operation** must, therefore be defined correctly unless the
**Standardization factor** Fstd is defined per trip.

If Fstd is defined per trip, then both **Fishing operations** and **Gears number per operation** must be set to 1.

### How do I check whether the simulation was OK?

* Open the simulation queue window – the status of the simulation should be **Simulation ended**.

* Select the simulation in the list and click **Show simulation log**.

* Check the boxes **Fatal**, **Error** and **Warn**. Il any messages are displayed, there was a problem. The exception
  trace stack is displayed under the error message. Look through the stack for a line for ISIS-Fish and see what has
  caused the problem.

### How do I interpret the headcount and biomass in the results window?

Beware! In the results window, the headcounts and biomasses for a population for each month are those calculated for the
end of the previous month.

### Why do I get a message warning me about the repository?

The message:
```
  You don’t use correct repository script for your application version 4.4.1.0.
  Do you want to switch your repository?
```

appears when you use a new version of ISIS-Fish. It is asking whether you wish to use the new versions of the scripts
that correspond to the new version of ISIS-Fish.

## Modeling tips

### How do I separate male and females if their growth patterns are different?

We can separate males from females by defining the length classes for the males as the low numbered groups and the
length classes for the females as the higher numbered groups.

For example to set up the length structure for the males:

  `60-70 70-77 77-110 110-166`

and for the females :

  `60-70 70-78 78-86`

in the **Population structure** tab, click **Recreate classes**, confirm and, in the next dialog box, select
**Input all the values**. In the following dialog box set **First min length** to “60” and set **Maximum group lengths**
to “70;77;110;166;70;78;86” and click **Finish**.

Group 4, the smallest female group, now has a minimum length of 166 and a maximum of 70. To correct this, go to the
**Group** tab, select group 4 in the Population group dropdown list and change the minimum **Length** to 60.

### How do I ignore the travel time in the fishing effort?

Set the vessel speed to a very high value, for example 10e9, this will make the travel time negligible.

### How to implement reproduction and recruitment?

See [Recruitement in V4.4](changes/changements44.html) page

## Model features

### What is the significance of the seasons for a population?

In ISIS-Fish, the seasons correspond to different parameterizations that depend on events affecting the population:
reproduction, change of age or length group, migration, etc.

To define the seasons, the various events should be fixed on a timeline and the seasons defined as consecutive months
between events.

### Which events are related to seasons and which to months of the year?

Unlike migration and change of age or length group, which take effect at the start of the season, reproduction can
occur in any month of the season. Seasons do not need to be defined for recruitment as fish can be recruited in any
month.

### N.B. The state variables are assumed to be uniform within a zone

One of the bases of ISIS-Fish simulation is that there is a single set of state variables for each zone.

Taking the example of a fishing zone defined for a metier (Zmet) that only covers part a zone defined for a population
(Zpop), at given timestep the population is uniformly distributed over Zpop and the fishing effort in Zmet will affect
the headcount of each age or length group across the whole of Zpop at the next timestep.:

  `(N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t)))`

An alternative would be simulation with the effort defined per cell so that the fishing mortality would be calculated
only for the population in the intersection. However, at the next timestep, the effect will be spread across the whole
of Zpop as the population is assumed to be uniform across the whole zone. :

  `(N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t)*(inter(Zmet,Zpop)/Zpop))`

As another example, consider the case of a zone Zp that has been declared as a marine protected area within Zpop leaving
an unprotected zone Zu.

* If the population is calculated for the whole of Zpop, rather than for Zp and Zu separately, then the population
  in Zp will be affected by the fishing mortality in Zu (at each timestep, the population is assumed to be uniform).:

    `N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t))`

  As the population is uniformly distributed across Zpop:

    `N(Zp,t+1) ~= N(Zpop,t+1)*Zp/Zpop`

* On the other hand, if the zones are separated, the population in Zp will not be affected by the fishing
  mortality in Zu (assuming that the fish in Zp are attached to their territory, there will be no migration):
```
    N(Zu,t+1) ~= N(Zu,t)-F(met)*N(Zu,t)
    N(Zp,t+1)= N(Zp,t+1)-exp(-M/12)*N(Zp,t)
```

* To fully describe a refuge, the migration from Zp to Zu should be defined as a function of the population densities.

Calculation for an MPA in a zone defined for a population Zpop

* Zpop is a single zone
* The protected area Zp is within Zpop
* The zone defined for the metier (Zmet) covers the whole of Zpop
* The fishing effort for the metier is reallocated to the unprotected part of Zpop:
```
    N(Zpop,t+1) = N(Zpop,t)-F(met)/(F(met)+M/12)exp(-F(met)+M/12)*N(Zpop,t)
    N(Zu,t+1) = N(Zpop,t+1)*Zu/Zpop, N(Zp,t+1) = N(Zpop,t+1)*Zp/Zpop
```
* If Zp is increased to k*Zp and the fishing mortality F(met) is unchanged:
```
    N(Zpop,t+1) = N(Zpop,t)-{F(met)/(F(met+M/12)*exp(-F(met)+M/12)}* N(Zpop,t)
    N(Zu,t+1) = N(Zpop,t+1)*[Zpop-k*Zp]/Zpop
    N(Zp,t+1) = N(Zpop,t+1)*k*Zp/Zpop
```
**It follows that, if the zone defined for the population is modeled as a single zone and, therefore, the population is uniformly distributed between the protected area and the unprotected area, changing the size of the protected area has no effect.**

If the fishing effort affects only part of the population within the unprotected area (with simulation of the fishing
effort per cell), then the size of the protected are will affect the catch and the abundance after fishing.:
```
  N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t)*(inter(Zmet,Zpop)/Zpop)).
```
In this case the difference in population when increasing the protected area from Zp to k*Zp will be:
```
  (k-1)*Zp/Zpop F(met)/(F(met)+M/12)*exp(-F(met)+M/12)}* N(Zpop,t)
```

### N.B. Overlapping population zones

It is possible to set up overlapping zones for a given population. Provided that the natural death rates in the two
zones are the same, the fishing mortality and catch in the overlap will be calculated correctly. If the natural death
rate is the same in the two zones, then, for a given metier, the fishing effort  E, the fishing mortality F and the
catch rate CR will also be the same.

* Population zone 1: E -> F -> CR
* Population zone 2: E -> F -> CR

The catch will be B1*CR + B2*CR = (B1+B2)*CR whether or not the calculation is carried out for each zone or each cell.


### Accessibility in ISIS

In ISIS, the accessibility is the biological contribution to the capturability of the population (Mahévas et al. 2001)
and the contributions of the boat and fishing gear to the capturability are expressed explicitly by the selectivity,
target factor, efficiency and other parameters. The accessibility parameter normally represents the changes in
capturability with changes in the spatial distribution. As ISIS-Fish is spatially explicit, these spatial variations are
modeled explicitly. This parameter takes account of behavioral factors affecting the capturability such as shoaling,
hiding in the sand, changes in vertical distribution, etc.

See
* Mahevas, S., Trenkel, V.M., Doray, M. and Peyronnet, A. 2011. Hake catchability by the french trawler fleet in the bay of biscay: estimating technical and biological components. ICES JOURNAL OF MARINE SCIENCE 68: pp. 107-118
* Laurec, A., et J. C. Le Guen. Dynamique des populations marines exploitées Tome1 : Concepts et Modèles. Centre National pour l’Exploitation des Océans, 1981. 602.
Gascuel: http://halieutique.agrocampus-ouest.fr/pdf/136.pdf
citing:
* Anonyme,  1979.  Monitoring  of  fish  stock  abundance  :  the  use  of  catch  and  effort  data.
FAO Fish. Tech. Paper. 155 : 101 p.
* Chadwick  M.,  R.N.  O'Boyle,  1990.  L'analyse  des  données  de  captures  et  d'effort.  In:
Méthodes d'évaluation des stocks halieutiques, Brêthes J.C., R.N. O'Boyle éd., Univ.
Québec à Rimouski, Vol. I et II, 77-101.


## Simulations

### What happens at t=0

The first timestep is rather special and not all events that would normally take place in January are handled

* Migration is handled
* Recruitment is handled
* Change of age or length group is not handled

N.B. The initial headcounts and results for January of the first year simulated are not comparable to those of other years.

### How do I carry out a simulation for each cell independently?

To calculate the model for each cell independently, rather than for each zone, the **SimulatorEffortByCell** simulator
should be selected in the **Advanced parameters** tab in the **Simulation launcher** and **Sensitivity analysis**
windows.

## Sensitivity analysis

### Sensitivity analyses do not run

The most likely error is that R is not correctly configured for ISIS-Fish.

At least Version 3.1.0 is required.

The following Windows environment variables should be configured

* `R_HOME` should be set to `C:\Program Files\R\R-3.1.0`
* `PATH` should include `%R_HOME%\bin`

## Tips and Tricks

## Defining population zones

1.	Keep in mind that a variable (abundance, effort,…) is assumed to be homogeneus within a zone
2.	Where?  Map of presence/absence of catch per month per group
3.	How many zones?  Map of density of cpue per month per group

### Implementing a growth function returning a mean length at month for an age-structure model

The default implementation of growth provides a mean length at year (individuals within an age group have the same
length during the year). It would be relevant to describe the growth within the year. This feature can be programmed
adding the month (current month of simulation = integer from 1 to 12)  to the age variable (age of the group = integer)
in the growth equation. An example of an equation returning a mean length at month given the age group:
```java
  // bargeo 2015, cm
  double Linf =  31.5;
  double K = 0.52;
  double T0 = -0.5;
  int mon = context.getSimulationControl().getStep().getMonth().getMonthNumber();
  double result = Linf*(1.0-Math.exp((-K*(((age+mon)/12)-T0))));
  return result;
```

### How do I fill a table using a .csv or .txt file?

Right click the table and select **Import/Export file CSV** in the context menu.

N.B. the values must be separated by semicolons.

### How do I specify large whole numbers?

Use 39e3 to set a floating point number rather than the integer 39000.

### Why does 41/1000 equal 0?

I divided 41 by 1000 in a rule and the result was 0, why?

In Java everything has a type. The two values 41 and 1000 are integers so Java performs an integer division with an
integer result (0). To have a floating point result (0.041) at least one of the two numbers must be a floating point
e.g. 41.0/1000.0.

In general, in Java, when an operation is carried out between two different types of number, Java selects higher
precision for the result. For example an operation between two integers gives an integer, while an operation between an
integer and a double or between two doubles gives a double. [Further explanation and examples](http://danshuster.com/apcs/java0105c.htm).

### How do I use an exponential (or other mathematical function) in an equation?

The Java Math class has most common mathematical functions as static methods.

[Beyond Basic Arithmetic Tutorial](https://docs.oracle.com/javase/tutorial/java/data/beyondmath.html)
