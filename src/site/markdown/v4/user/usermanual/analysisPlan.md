# Simulation plans

Simulation plans are used to sequence several simulations. Each simulation can be parameterized using the results from
previous simulations or using random values.

Simulation plans may be combined, for example to use a plan specific to a project combined with the standard plan Max
which limits the number of simulations.

## General

A simulation plan is a Java class providing the standard methods of the SimulationPlan interface.

* `getDescription()` – Returns a String describing the plan.
* `getNecessaryResult()`– Returns a string array with the results required for the plan.
* `init()` – Called once only at the start of the process.
* `beforeSimulation()` – Called before each simulation.
* `afterSimulation()` – Called after each simulation.

`void init(SimulationPlanContext context)` is called to initialize values required for the whole plan. It can also set
values specific to the simulation and open dialog boxes for requesting additional settings.

`String[] getNecessaryResult(SimulationPlanContext context)` is called to define the results required by the plan. It
should return an array of strings identifying the results.

`boolean beforeSimulation(SimulationPlanContext context, SimulationStorage nextSimulation)` is called to set the
simulation and model parameters for the next simulation.  `SimulationPlanContext` and `SimulationStorage` are
described in the [ISIS-Fish Javadoc](../../apidocs/index.html) (API). `beforeSimulation()` should return `false` to stop the simulation
process – if there is more than one plan, then `beforeSimulation()` for the following plans will not be called.

`boolean afterSimulation(SimulationPlanContext context, SimulationStorage lastSimulation)` is called after each
simulation. `afterSimulation()` should return `false` to stop the simulation process, however `afterSimulation()`
will still be called for all the plans.

Simulation plans can have parameters that are set in the simulation plan tab in the GUI. They should be a basic Java
type (int, double, …), Timestep, Month or ISIS-Fish object (Metier, Strategy), the name should be prefixed with
``param_`` and they should be declared ``public``, preferably preceded by an ``@Doc`` description.

## Architectural principals

The design choice of having before and after methods rather than having a loop in the plan calling the simulation
method, was made so that if a simulation plan is in the queue executing and another, higher priority simulation is run,
the queued simulation process will be stopped to give priority to the new simulation. This would not be possible if the
simulation plan controlled the process.

## Basic example

This example is a simulation plan that limits the number of iterations
```java
    public class Max implements SimulationPlan {

    @Doc(value = "Maximum number of simulations")
    public int param_max = 10;

        public String getDescription() throws Exception {
            return t("Limits the number of simulations");
        }

        public String[] necessaryResult = {};

        public String[] getNecessaryResult(SimulationPlanContext context) throws Exception {
            return this.necessaryResult;
        }

        public void init(SimulationPlanContext context) throws Exception {
        }

        public boolean beforeSimulation(SimulationPlanContext context, SimulationStorage nextSimulation) throws Exception {
            return nextSimulation.getParameter().getSimulationPlanNumber() < param_max;
        }

        public boolean afterSimulation(SimulationPlanContext context, SimulationStorage lastSimulation) throws Exception {
            return true;
        }
    }
```

This plan could, for example, be used with a plan that modifies a parameter randomly and indefinitely.

## Modifying parameters for the next simulation

Parameters to be modified should be retrieved as ``SimulationParameter`` objects using the method
``nextSimulation.getParameter()``. These classes are described in the [ISIS-Fish Javadoc](../../apidocs/index.html) (API).

## Retrieving a previous simulation in the plan

A previous simulation can be retrieved from the ``context`` parameter.
```java
  // retrieve simulation n
  SimulationStorage prevSim = context.getSimulation(n);
```

N.B. The `context.getNumber()` method for retrieving the current simulation number is deprecated as it is ambiguous.
Use
```java
  nextSimulation.getParameter().getSimulationPlanNumber()
```
to get the next simulation number in the `beforeSimulation()` method and
```java
  lastSimulation.getParameter().getSimulationPlanNumber()
```

to get the previous simulation number in the afterSimulation() method.

## Retrieving the results of a simulation

The results from a simulation can be recovered from the `SimulationStorage` object using the ``getResultStorage()``
method.
```java
  ResultStorage results = prevSim.getResultStorage();
  MatrixND n = results.getMatrix(ResultName.MATRIX_ABUNDANCE);
```

## Modifying the database for the next simulation

References to objects in the database can be retrieved using `IsisFishDAOHelper` static methods.

```java
    TopiaContext db = nextSimulation.getStorage();
    MetierDAO dao = IsisFishDAOHelper.getMetierDAO(db);
    Metier metier = dao.findByName("Bank herring");
    metier.setGearParameterValue("30");
```

## Complete beforeSimulation method modifying the database

This example sets different mesh sizes for three simulations and then stops.

```java
    public boolean next(SimulationPlanContext context, SimulationStorage nextSimulation) throws Exception {
      String [] values = new String[]{"10", "20", "30"};
      int number = nextSimulation.getParameter().getSimulationPlanNumber();

      if (number < values.length) {
        TopiaContext db = nextSimulation.getStorage().beginTransaction();
        MetierDAO dao = IsisFishDAOHelper.getMetierDAO(db);
        Metier metier = dao.findByName("Bank herring");
        metier.setGearParameterValue(values[number]);
        db.commitTransaction();
        db.closeContext();
        return true;
      } else {
        return false;
      }
    }
```

## Tutorial and examples of simulation plans for sensitivity analysis and calibration

## Checklist before running a simulation plan
Check

* That the initial population counts have been set
* That the rules (other than those modified by the plan) have be defined
* That the name of the simulation is meaningful

  Note. If an experimental design matrix is used and the plan starts in the middle of the matrix it may be useful if the
  first line in the matrix is included in the name. For example if the name MPA_54 is use for a plan starting at row 54
  of the matrix, then ISIS-Fish will name the simulation N “MPA_54 yyyy-mm-dd-hh-mm_N and adding 54 and N will give you
  the corresponding row of the matrix.

* The parameters for the plan are correctly set, in particular the start row in the matrix and the directory for the
  files when an experimental design matrix is used.
* That the results to be exported and saved have been defined and that the export directory has been correctly
  configured.
* That the duration of the simulation is set.

GO!
