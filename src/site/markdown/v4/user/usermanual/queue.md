# Simulation queue

The simulation queue lists all the simulations running, and their progression, the simulations that have completed and
the simulations that are queued waiting to run.

<img src="simulation_08_tabQueue.png" />

1. List of simulations that are queued or running.
2. List of simulations that have terminated.
3. **Pause/Resume** – Used to pause or resume all simulations running
4. **Stop simulation** – Stops the simulation selected in the list of queued simulations, Item 1.
5. **Restart simulation** – Restarts a failed simulation. The simulation must be selected in the list of terminated
   simulations, Item 2.
6. **Show simulation log** – Opens a log console for the simulation selected in the list of terminated simulations,
   Item 2.
7. **Clear done jobs** – Clears the list of terminated simulations, Item 2.

## Simulation log console

<img src="simulation_dialog_01_consoleLog.png" />

1. **Log message levels** – Particular message levels cans be selected. If more than one level is selected, then the
   messages for all the levels selected will be displayed.

   By default, no levels are selected which is equivalent to all levels selected.
2. **Filter** – The messages may be filtered using a word.
3. **Apply filter** – When the filter is applied, only those messages which include the word will be displayed.

   Both the filter and the selection of message levels apply.

4. **Reset** – clears both the filter and the selection of message levels.