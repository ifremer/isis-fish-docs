<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# User interface

## Main Window

The main window has a set of six buttons down the left hand side. Each button is used to access different ISIS-Fish
modules.

<img src="mainWindow.png" />

The buttons display sub-windows to access :

* Region (fishery) definition
* Simulation control
* Sensitivity analysis control
* Presentation of results
* Editor for scripts, rules, exports, simulation plan
* Simulation queue

Each of these sub-windows can also be opened in a new window using the **Window** menu.

### File menu

The top menu item **Server synchronization** is used to synchronize local copies of scripts with the scripts on the
server.

The second menu item **Close** closes ISIS_Fish.

### Window menu

The six menu items correspond to the six buttons (but not necessarily in the same order). Each opens the corresponding
sub-window in a new window.

### Configuration menu

The **Configuration** menu items are used to set the configuration for various ISIS-Fish features.

* **Configuration** to set general user preferences.
* **VCS configuration** to set the version control system parameters
* **Carparmor configuration** to set up the local version of ISIS-Fish to use the Carparmor supercomputer.
* **R configuration** to test if R is correctly configured

### Help menu

The **Help** menu provides

* **ISIS-Fish Website** link to this website
* **API** to access the various application programming interfaces
* **About** with version and license information


### Status bar

The status bar at the bottom of the window shows

* the progress bar for the current operation on the left
* a message area in the middle
* a screenshot button, the current ISIS-Fish memory use / memory allocated and the time of day on the right


## Simulation window

This window is used to set up and execute simulations.

<img src="simulationTab.png" />

This window has a number of tabs

* **Parameters** – for setting the basic simulation parameters (name, description, region, strategies, initial
  populations, rules, number of months to simulate) as well as for launching the simulation process.
* **Pre simulation script** – for defining a pre-simulation script which will be executed to modify the database before
  any simulations are run. This tab is activated when **Use pre simulation script** is checked in the parameters window.
* **Simulation plan** – for defining a simulation plan to carry out multiple simulations. This tab is activated when
  **Use simulation plan** is checked in the parameters window.
* **Optimization** – for defining the objective function, the optimization method, exported values and observations.
  This tab is activated when **Use optimization method** is checked in the parameters window.
* **Results export** – for selecting scripts for exporting results.
* **Result choice** – for selecting results to be kept for viewing in the **Results** window.
* **Advanced parameters** – for setting parameters such as the simulator to be used, cache control, trace levels and
  **Free parameters** than can be used in rules, the simulator, etc.

If a rule or export script requires a result that is not selected, then the result required is added automatically to
the list results selected.

When a simulation is run, the local timestamp, yyyy-mm-dd-hh-mm, is added to the simulation name.

## Results window

This window displays the results of a simulation in graphical, cartographical or tabular formats

<img src="resultTab.png" />

Click **Open** to display the results of the most recent simulation or, to display the results from a previous
simulation, select the simulation results required from the dropdown list at the top of the window and click **Open**
to open the results in a new sub-window.

<img src="resultWindowGraph.png" />

<img src="resultWindowMap.png" />

## Region window

This window is used to display and modify regions.

Select the region required from the dropdown list at the top of the window.

<img src="inputTab.png" />

<img src="inputTabRegion.png" />

## Scripts window

This window is used to edit any type of script

* scripts
* simulators
* exports
* rules
* simulation plans
* model formulae
* sensitivity analyses

<img src="scriptTab.png" />

All scripts are stored as text files in the filing system. Any text or source file editor can, therefore, be used to
edit them.

## Sensitivity analysis window

This tab is used to parameterize and run a sensitivity analysis as well as display the results.

<img src="sensitivityTab.png" />

This tab is itself comprised of a number of tabs

* **Parameters** – to set the basic parameters: identifier, description, region, strategies, populations, rules, number of years to simulate
* **Sensitivity analysis** – to set the factors to be studied
* **Sensitivity method** – to set select and parameterize the method used
* **Results export** – this tab is used to select the export scripts and directory
* **Result choice** – this tab is used to select the results to be kept for visualization using the **Analyze results** tab
* **Advanced parameters** – this tab is used to select the simulator to be used, control the cache, control the
  statistics, filter the log and set free parameters for use by rules or the simulator, etc
* **Analyze results** – this tab is used to display the results of a sensitivity analysis

If a rule or export script requires a result that is not selected, then the result required is added automatically to
the list results selected.

When a simulation is run, the local timestamp, yyyy-mm-dd-hh-mm, is added to the analysis name. For each simulation the
name followed by an underscore followed by the sequence number of the simulation within the sensitivity analysis.
