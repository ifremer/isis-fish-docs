# Presentation of results

The results window presents a summary of the simulation and the results in graphical, cartographic or tabular format. It
can also be used to delete a simulation or view the simulation log.

## Results window

<img src="result_00.png" />

1. **Simulation** – This lists all the simulations executed locally.
2. **Filter** – Filter for the simulation list.

   The button to the right of the filter cancels the current simulation filter and refreshes list of simulations.
3. **Open** – When a simulation has been selected, click Open to display the summary of the results in the results
   frame, Item 6.
4. **Remove** – Deletes the selected simulation.
5. **Show Logs** – Displays the log for the selected simulation.
6. **Results Frame** – The frame for displaying the results.

## Results summary

<img src="result_01.png" />

1. **Results selection bar** – The type of results to be displayed are selected using the radio buttons. When a
   simulation is loaded, the summary is selected.
2. **Result** – Dropdown list of all the result matrices from the simulation
3. **Export menu** – This menu is used to save a export from the simulation. When an export is selected, a dialog box is
   opened to specify the file name and directory.
4. **Date** – Select the period of interest.
5. **Headers for dimension 1** – Select the data range of interest.
6. **Headers for dimension 2** – Select the data range of interest.
7. **Sum across period** – Sum the data across the period.
8. **Sum across year** – Sum the data across the year.
9. **Sum across dimension 1** – Sum the data across dimension 1.
10. **Sum across dimension 2** – Sum the data across dimension 2.
11. **Display** – Displays the data selected on the left.
12. **Summary** – The summary of the simulation.

## Graphical presentation of the results

<img src="result_02.png" />

1. **Display** – Displays the data selected on the left.
2. **Chart** – Displays the data as a chart.
3. The results displayed as a chart.
4. Dropdown list of graphical display formats.

A right click on the chart displays a context menu for setting the chart properties (title, labels, format, etc.) saving
the chart as an image or printing it, zooming, etc.

## Cartographic presentation of the results

<img src="result_03.png" />

1. **Map** – Displays the results on the map of the region.
2. The map of the region with shades indicating the results in each cell.

## Tabular presentation of the results

<img src="result_04.png" />

1. **Matrix** – Displays the results matrix in a table.
2. The results matrix.
3. **Export as CSV** – Exports the results matrix as a CSV file. A dialog box is used to set the directory and filename.
