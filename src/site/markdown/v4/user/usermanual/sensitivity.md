# Sensitivity analysis

<img src="tip.png" /> This module is used to create, launch and use the results of a sensitivity analysis.

## Window layout

<img src="sensitivity_00.png" />

1. The menu bars
2. The tabs for selecting the various pages for setting the sensitivity analysis parameters and for analyzing the results
3. The contents of each tab
4. The status area

## Menus

The menus carry out global actions for the sensitivity analysis.

### Simulation menu

<img src="sensitivity_01_menuSimulation.png" />

1. **Save simulation parameters** – to save the all the simulation parameters for future use.
2. **Restore simulation parameters** – to load simulation parameters previously saved.

## Setting up a sensitivity analysis

A sensitivity analysis involves a number of stages.

1. **Parameters tab** – Simulation parameters
2. **Sensitivity analysis tab** – Defining the factors
3. **Sensitivity method tab** – Defining the method for exploring the factor space and the outputs to be examined
4. **Results export tab** – Selecting results to export
5. **Result choice tab** – Selecting results to save for analysis
6. **Advanced parameters tab** – Setting advanced parameters for the sensitivity analysis
7. **Analyze results tab** – Analyzing the results of the sensitivity analysis


### Parameters tab – Simulation parameters

<img src="sensitivity_02_parameters.png" />

The Parameters tab is used for setting the basic simulation parameters.

1. **Load old simulation** – This lists all the simulations executed locally. The list is empty when ISIS-Fish is
   executed for the first time. When a simulation is run successfully, it will be added to this list the next time
   ISIS-Fish is run.
2. **Filter** – Filter for the list of simulations.
3. **Refresh list** (to the right of the filter) – Cancels the current simulation filter and refreshes the list of
   simulations.
4. **Sensitivity name** – The name of the sensitivity analysis.

   If you load a previous simulation, the name will appear here.

   <img src="tip.png" /> A new simulation can be created easily from a previous simulation by changing the name.

5. **Description** – The description of the sensitivity analysis.

   If you load a previous simulation, the description will appear here.
6. **Region** – Select the region for the sensitivity analysis.

   Loading the region will fill in the list of strategies and the list of populations.

   <img src="tip.png" /> Always select the region before defining the rules as these are linked to the objects in the region.
7. **Strategies** – When the region has been loaded, all the strategies defined for the region will be listed here. The
   strategies selected in this list will be used by the simulator.
8. **Populations** – When the region has been loaded, all the populations defined for the region will be listed here.

   When a population is selected, the initial counts for age or length groups in each population zone are displayed in
   the table, Item 9.

   These are used to initialize the simulator.

9. **Initial population** – This table holds the initial counts for each age or length group in each zone for the
   population selected.
10. **Number of months** – This specifies the period over which the simulation is carried out.
11. **Simulate** – Runs the simulation as specified by Simulation launcher.
12. **Simulation launcher** – Specifies where the simulations will be run:

    1. **in subprocess** – the simulations will be run in subprocesses. If several simulations are run at the same time,
      each will run in its own subprocess and so, potentially, each in parallel on a separate core.
    2. **on Caparmor server** – the simulations will be run on the Ifremer Caparmor supercomputer.
      See Installing ISIS-Fish on Caparmor for details of the configuration required.
    3. **in current process** – the simulations will be run in the current process.

<img src="sensitivity_02_parameters_rules.png" />

1. **Available rules** – This is a list of rules that have been defined in ISIS-Fish. Each rule is followed by its
   description.
2. **Selected rules** – This is the list of rules that have been selected for the simulation. Each rule is followed by
   its description.

   <img src="tip.png" /> When a rule is selected in this list, the parameters are displayed in the table below.
3. **Add** – Adds the rules selected in the Available rules, Item 10, to the Selected rules, Item 11.
   As each rule is added, a dialog box is displayed for setting the parameters.
4. **Remove** – Removes the rules selected from the Selected rules, Item 11.
5. **Clear** – Removes all the rules from the Selected rules, Item 11.
6. **Rule parameters** – When a rule in the Selected rules list is selected, the parameters are displayed in this table
   and may be modified.

   <img src="tip.png" /> When you move the pointer over a parameter name, a tooltip will appear with the description of the parameter.


### Sensitivity analysis tab – Defining the factors

<img src="sensitivity_02_sensitivityFactors.png" />

The sensitivity analysis tab has the object tree for the region for select objects and their parameters that will be
used as factors in the sensitivity analysis.

1. **Object tree for the region** – For selecting an object with one or more parameters to be used as factors.
2. **Factors that have been selected** – As each factor is selected it is added to this list.

   Double clicking a factor opens the dialog box for setting the parameters for the factor. A right click opens a
   context menu for deleting the factor
3. **Parameter that can be used as a factor** – This icon indicates the parameter can be used as a factor. It is
   animated to indicate that the pointer is hovering over the field for potential factor. Clicking the field opens a
   dialog box for setting the parameters for the factor.


#### Defining a continuous factor for a parameter specified as a value

When a parameter defined as a value is used as a factor, the value may either be continuously variable or be one of a
set of discrete values. For a continuously variable factor, the distribution must be specified.

<img src="sensitivity_02_sensitivityFactorsContinuousNumber.png" />

1. **Continuous factor** – Selected if the factor is continuously variable.
2. **Distribution** – Dropdown list of probability distributions.
3. **First parameter** – The first parameter for the distribution selected – Minimum for QUnif Min/Max.
4. **Last parameter** – The last parameter for the distribution selected – Maximum for QUnif Min/Max.
5. **Comment** – Any useful information about the factor.
6. **Cancel** – Throw away the modifications made. If it is a new factor, then it will not be created.
7. **Save** – Saves the parameters for the factor. If it is a new factor, it will be added to the list of factors
   selected.
8. **Remove** (not visible when adding a new factor) – Removes the factor from the analysis.

#### Defining a discrete factor for a parameter specified as a value

<img src="sensitivity_02_sensitivityFactorsDiscreteNumber.png" />

1. **Finite discrete factor** – Selected if the factor is takes discrete values.
2. **Number of values** – The number of different values for the factor.
3. **Ok** – Click to set up a tab for each different value. Each different value should then be set in its own tab.
4. **Tabs for the values** – For selecting each value.
5. **Value for each tab** – Each tab defines one of the different values for the factor.  Each different value should be
   set as required.
6. **Comment** – Any useful information about the factor
7. **Cancel** – Throw away the modifications made. If it is a new factor, then it will not be created.
8. **Save** – Saves the parameters for the factor. If it is a new factor, it will be added to the list of factors
   selected.
9. **Remove** (not visible when adding a new factor) – Removes the factor from the analysis.

#### Defining a continuous factor for a parameter specified as an equation

<img src="sensitivity_02_sensitivityFactorsContinuousEquation.png" />

1. **Continuous factor** – Selected if the factor is continuously variable.
2. **Variable name** – The variable should be a constant in the equation. When the factor is defined the line in the
   equation defining the constant is modified so that the value can be changed during the analysis. Note. If the factor
   is a variable that exists only for sensitivity analysis, then this variable should be added to the equation with a
   default value (e.g. double Ktemp = 1) and the equation changed to use this value.
3. **Distribution** – Dropdown list of probability distributions.
4. **First parameter** – The first parameter for the distribution selected – Reference for QUnif%, usually the default
   in the equation.
5. **Last parameter** – The last parameter for the distribution selected – Coefficient for QUnif%.
6. **Valid variable** – Checks that the constant is defined in the equation, modifies the equation to vary this constant
   and saves the parameters. The modification to the equation does not affect its use in other simulations that may be
   carried out.

   <img src="tip.png" /> A single equation may have a factor for any or all of the constants in the equation. ISIS-Fish will create a
   factor for each constant added in this dialogue box.
7. **Add** – Adds a new factor.
8. **Remove** – Removes the factor selected.
9. **Comment** – Any useful information about the factor or factors.
10. **Cancel** – Throw away the modifications made. If the dialogue box was opened to create a new factor, then no new
   factors will be created.
11. **Save** – Saves the parameters for the factor or factors. If the dialogue box was opened to create a new factor,
   the new factor or factors will be added to the list of factors selected.
12. **Remove** (not visible when adding a new factor) – Removes the factor from the analysis.


#### Defining a discrete factor for a parameter specified as an equation

<img src="sensitivity_02_sensitivityFactorsDiscreteEquation.png" />

1. **Finite discrete factor** – Selected if the factor is takes discrete values.
2. **Number of values** – The number of different values for the factor.
3. **Ok** – Click to set up a tab for each different value. The equation for each different value should then be set in
   its own tab.
4. **Tabs for the values** – For selecting each equation.
5. **Equation for each tab** – Each tab defines one of the different values for the factor.  Each equation should be
   modified as required.
6. **Comment** – Any useful information about the factor
7. **Cancel** – Throw away the modifications made. If it is a new factor, then it will not be created.
8. **Save** – Saves the parameters for the factor. If it is a new factor, it will be added to the list of factors
   selected.
9. **Remove** (not visible when adding a new factor) – Removes the factor from the analysis.


#### Defining a continuous factor for a parameter specified as a matrix

<img src="sensitivity_02_sensitivityFactorsContinuousMatrix.png" />

1. **Continuous factor** – Selected if the factor is continuously variable.
2. **Distribution** – Dropdown list of probability distributions: only QUnif% is available.
3. **reference** – These are the nominal values from the matrix which may be modified.
4. **coefficient** – the range of values defined as a ± percentage which is applied to each value in the table.
5. **Comment** – Any useful information about the factor.
6. **Cancel** – Throw away the modifications made. If it is a new factor, then it will not be created.
7. **Save** – Saves the parameters for the factor. If it is a new factor, it will be added to the list of factors
   selected.
8. **Remove** (not visible when adding a new factor) – Removes the factor from the analysis.

#### Defining a discrete factor for a parameter specified as a matrix

<img src="sensitivity_02_sensitivityFactorsDiscreteMatrix.png" />

1. **Finite discrete factor** – Selected if the factor is takes discrete values.
2. **Number of values** – The number of different values for the factor.
3. **Ok** – Click to set up a tab for each different value. The matrix for each different value should then be set in
   its own tab.
4. **Tabs for the values** – For selecting each matrix .
5. **Matrix for each tab** – Each tab defines one of the different values for the factor.  Each matrix should be
   modified as required.
6. **Comment** – Any useful information about the factor
7. **Cancel** – Throw away the modifications made. If it is a new factor, then it will not be created.
8. **Save** – Saves the parameters for the factor. If it is a new factor, it will be added to the list of factors
   selected.
9. **Remove** (not visible when adding a new factor) – Removes the factor from the analysis.

### Sensitivity method tab – Defining the method for exploring the factor space and the outputs to be examined

<img src="sensitivity_04_sensitivityMethod.png" />

1. **Method** – A dropdown list of all the methods available for sensitivity analysis.
2. **Parameters** – The parameters for the method. The fields are preset with default values which may not be suitable
   for the case being studied. Always check the values of these parameters.
3. **Cardinality** – Some methods require discrete values for the factors. In this case, specify the number of discrete
   values for each factor. This table is only visible if the factors must have discrete values.
4. **Exports available** – List of sensitivity exports that are available. Multiple exports may be selected.
5. **Exports assigned** – List of exports that have been assigned to the sensitivity analysis.
6. **Add** – Add the exports selected from the exports available to the exports assigned.
7. **Remove** – Remove the selected exports from the exports assigned.
8. **Clear** – Remove all the exports from the exports assigned.
9. **Export parameters** – Lists the parameters for the export selected in the exports assigned list. These parameters
   should be set correctly for a correct sensitivity analysis.

### Results export tab – Selecting results to export

<img src="sensitivity_05_tabExportResult.png" />

1. **List of exportable results** – The results to be exported at the end of the simulation should be selected from this
   list of all export scripts defined.
2. **Save for next simulation** – saves the selections so that the next time the simulation launcher is run, the
   selected exports will be selected again.

### Result choice tab – Selecting results to save for analysis

<img src="sensitivity_06_tabResultChoice.png" />

1. **List of results available** – The results required from the simulation should be selected from this list of all
   results scripts defined.
2. **Only keep results for first simulation** – If this box is checked, the results from the first simulation will be
   kept.
3. **Save for next simulation** – saves the selections so that the next time the simulation launcher is run, the
   selected results will be selected again.

### Advanced parameters tab – Setting advanced parameters for the sensitivity analysis

<img src="sensitivity_07_tabAdvancedParams.png" />

1. **Simulator to use** – This sets the basic simulator configuration.

   1. Dropdown list of simulators available.
   2. **Simulation statistics** – Check the box to generate simulation statistics.
   3. **Simulation cache** – Check the box to use a simulation cache.
2. **Log filter settings** – Sets the log levels for deferent parts of ISIS-Fish.

   1. **Simulator log level** – sets the log level for the simulator itself.
   2. **Script log level** – sets the log level for scripts
   3. **Library log level** – sets the log level all other parts of ISIS-Fish
3. **Free parameters** – The top part of the Free parameters frame is used to define new free parameters. After setting
   the Tag and the Value, click Add to add the parameter to the list, Item 4.
4. **Free parameter list** – This lists all the free parameters that have been defined
5. **Remove** – Removes the free parameter selected (if one is selected) from the list.
6. **Save for next simulation** – saves the settings so that the next time the simulation launcher is run, the settings
   will be reloaded.

### Analyze results tab – Analyzing the results of the sensitivity analysis

<img src="sensitivity_08_results.png" />

1. **List of sensitivity analysis** – A list of all the sensitivity analyses known. N.B. This may include analyses that
   have not yet been carried out.
2. **Analyze results** – Run the analysis scripts on the results of the simulations and display the analysis in the
   analysis frame.

   |tip| The results are always analyzed after the last simulation has terminated. This button reruns the analyses on
   the results of the simulations without having to rerun the simulations themselves. The means that the sensitivity
   analysis scripts can be modified after the simulations have been run.
3. **Display results** – Display the results of the sensitivity analysis.
4. **Analysis frame** – Displays the results of the sensitivity analysis. Only those results that are exported by the
   sensitivity analysis scripts are shown.

### Using the results in R

The sensitivity analysis scripts use R for the calculations. The objects used are standard and the R session is saved.
The results can, therefore, be used in R for further processing.

#### Finding the .RData file

The results are in the `SensitivityAnalysisName.RData` file in the `isis-export` directory defined in the
configuration file.

#### Contents of the .RData file

The `.RData` file holds a session with numerous R objects from the various sensitivity analysis stages.

##### `isis.factor`

`isis.factor` is a 5 column, single row data frame.

* column 1: `nomFacteur`
* column 2: `Nominal` (value in the database)
* column 3: `Continu` (TRUE/FALSE)
* column 4: `Binf` (minimum value)
* column 5: `Bsup` (maximum value if continuous, otherwise the number of values)

Attributes

* an attribute for each discrete factor: `nomFacteur`: list(values)
* an attribute `nomModel`: “isis-fish-externeR”

`isis.factor` is registered in R as `SensitivityAnalysisName_0.isis.factor` (all spaces are stripped by R).

##### `isis.factor.distribution`

`isis.factor.distribution` is a 3 column data frame with one row per factor

* column 1: `NomFacteur`
* column 2: `NomDistribution`
* column 3: `ParametreDistribution`

`isis.factor.distribution` is registered in R as `SensitivityAnalysisName_0.isis.factor.distribution` (all spaces
are stripped by R).

##### `isis.methodExp`

`isis.methodExp` is a list with three objects

* object 1: `isis.factor`
* object 2: `isis.factor.distribution`
* object 3: `call`

Attributes

* an attribute `nomModel`: “isis-fish-externeR”

`isis.methodExp` is registered in R as `SensitivityAnalysisName_0.isis.methodExp` (all spaces are stripped by R).

##### `isis.simule`

`isis.simule` is a data frame with one row per simulation and one column for each factor and each simulation result

* columns 1 to k: values of the k factors.
* columns k+1 to k+n: values of the n simulation results

Attributes

* an attribute `nomModel`: “isis-fish-externeR”
* an attribute `call`: the method that generating the simulations.

`isis.simule` is registered in R as `SensitivityAnalysisName_0.isis.simule` (all spaces are stripped by R).

##### `isis.methodAnalyse`

`isis.methodAnalyse` is a list with 5 objects:

* object 1: `isis.factor`
* object 2: `isis.factor.distribution`
* object 3: `isis.simule`
* object 4: `call_method`
* object 5: `analysis_result` (R object with the analysis results. If the results were calculated by aov, the object
  is a list with the aov and the sensitivity indices.)

`isis.methodAnalyse` is registered in R as `SensitivityAnalysisName_0.ResultName.isis.methodAnalyse` (all spaces are stripped by R).

##### List of all R session objects

You can always use the R function ``ls()`` to list all the objects in the R session.
