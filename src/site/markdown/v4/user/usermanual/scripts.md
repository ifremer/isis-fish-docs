# Script editor

The script editor is used to create, edit and delete the local copies of scripts used by ISIS-Fish.

It can also be used to retrieve new scripts from the server and submit scripts to the server. For these operations the
repository must be correctly configured (Menu **Configuration / Configuration / Official VCS** and
**Configuration / Configuration / Community VCS**)

The editor also checks and compiles the scripts.

## Window layout

<img src="script_00.png" />

1. **Menu bar** – All the actions on scripts.
2. **New** – Creates a new script. The type of script is selected from the dropdown list of all the types of script used
   by ISIS-Fish. New scripts are added to the **Community** section of the script tree, Item 4.
3. **Button bar** – Common actions on scripts.
4. **Script tree** – Arranged by repository (**Official** or **Community**) / type (/ subtype)  / name.
5. **Script editing frame** – Displays the script selected in the tree for editing.
6. **Log** – Lists the messages from script compilation, diff, etc.
7. Status bar.

## Menu bar

### File menu

<img src="script_01_menuFile.png" />

1. **New** – Creates a new script. This action can also be invoked using the **New** button.

   A dialog box opens to specify the name of the new script.

   N.B. A script name must start with an uppercase letter and may contain only alphanumeric and “_”.

   Currently only “equations” (formulae) have subtypes – to create a new equation prefix the name with the subtype and
   “/” (e.g. Migration/Char).

   If the name is valid, the new script has been created locally, it is inserted into the script tree, selected and
   displayed in the script editing frame.
2. **Save** – Save the modifications made to the local copy of the script.

   This action can also be invoked using the **Save** button.

3. **Delete locally** – Deletes the local copy of a script. The copy of the script in the repository is not affected.

   Use with care!


### Edit menu

The Edit menu has all the basic editing functions for the script in the script editing frame.

They can only be used when a script has been selected.

Some of these actions are also available on buttons.

<img src="script_07_menuEdit.png" />

1. **Cut** – Cuts out the selection and copies it to the clipboard.
2. **Copy** – Copies the selection to the clipboard.
3. **Paste** – Pastes a selection from the clipboard into the script.

### Server menu

The Server menu groups the actions on a copy of the script in the repository.

A local script must be selected.

<img src="script_08_menuServer.png" />

1. **Commit script to server** – Sends a script that has been modified locally to the repository. This action can also
   be invoked using the **Commit** button.

   A Dialog box opens for a comment on the changes.

   You must have write access to the repository.
2. **Display diff with server version** – Displays the difference between the version saved locally and the version in
   the repository.
3. **Delete locally and on server** – Deletes both the local and repository copies of the script.

   You must have write access to the repository.

   Use with care!


### Code menu

The Code menu invokes the Java compiler and utilities.

<img src="script_09_menuCode.png" />

1. **Check syntax** – Compiles the selected script. The compiler messages are displayed in the log. Note that there may
   warning messages even if the script compiles OK.

   This action can also be invoked using the **Check** button.
2. **Check all scripts** – As above, but checks all the local copies of the scripts.
3. **Evaluate** – Calls the main() method of the selected script.

   If necessary, the script is compiled first.

   This is only possible if main() is static and takes a single String array parameter.

   This action can also be invoked using the **Evaluate** button.
4. **Generate script javadoc** – Generates the Javadoc for all scripts.
5. **Show script javadoc** – Opens the Javadoc in the default browser.


## Additional screenshots

<img src="script_02_menuFile_new.png" />

Creating a script using the **File/New** menu.

<img src="script_03_combo_new.png" />

Creating a script using the **New** button.

<img src="script_04_new.png" />

Editing the TailleMin rule.

<img src="script_10_verifSyntax_ok.png" />

TailleMin compiled OK.

<img src="script_11_verifSyntax_ko.png" />

TailleMin compiled with an error.