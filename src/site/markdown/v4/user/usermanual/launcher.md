# Simulation Launcher

<img src="tip.png" /> This module is used to launch a simulation, or set of simulations, using ISIS-Fish scripts (see [Scripts](inputs.html)) applied to a
region that has been set up (see [Defining regions]()).

## Introduction

The simulation launcher has a set of tabs for configuring and launching a simulation or set of simulations.

* **Parameters** – for setting the basic simulation parameters (name, description, region, strategies, initial
  populations, rules, number of months to simulate) as well as for launching the simulation process.
* **Pre simulation script** – for defining a pre-simulation script which will be executed to modify the database before
  any simulations are run. This tab is activated when **Use pre simulation script** is checked in the parameters window.
* **Simulation plan** – for defining a simulation plan to carry out multiple simulations. This tab is activated when
  **Use simulation plan** is checked in the parameters window.
* **Optimization** – for defining the objective function, the optimization method, exported values and observations.
  This tab is activated when **Use optimization method** is checked in the parameters window.
* **Results export** – for selecting scripts for exporting results.
* **Result choice** – for selecting results to be kept for viewing in the **Results** window.
* **Advanced parameters** – for setting parameters such as the simulator to be used, cache control, trace levels and
  **Free parameters** than can be used in rules, the simulator, etc.

If a rule or export script requires a result that is not selected, then the result required is added automatically to
the list results selected.

When a simulation is run, the local timestamp, yyyy-mm-dd-hh-mm, is added to the simulation name.

## The user interface

### Window layout

<img src="simulation_00.png" />

1. The **Simulation** menu
   * **Save simulation parameters** – to save the all the simulation parameters for future use.
   * **Restore simulation parameters** – to load simulation parameters previously saved.
2. The tabs for selecting the various pages for setting the simulation parameters
3. The contents of each tab
4. The status area

### Parameters tab

<img src="simulation_01_tabParams.png" />

The **Parameters** tab is used for setting the basic simulation parameters.

1. **Load old simulation** – This lists all the simulations executed locally. The list is empty when ISIS-Fish is executed
   for the first time. When a simulation is run successfully, it will be added to this list the next time ISIS-Fish is
   run.
2. **Filter** – Filter for the list of simulations.
3. **Refresh list** – Cancels the current simulation filter and refreshes the list of simulations.
4. **Simulation name** – The name of the simulation.

   If you load a previous simulation, the name will appear here.

   <img src="tip.png" /> A new simulation can be created easily from a previous simulation by changing the name.
5. **Description** – The description of the simulation.

   If you load a previous simulation, the description will appear here.
6. **Region** – Select the region for the simulation.

   Loading the region will fill in the list of strategies and the list of populations.

   <img src="tip.png" /> Always select the region before defining the rules as these are linked to the objects in the region.
7. **Strategies** – When the region has been loaded, all the strategies defined for the region will be listed here. The
   strategies selected in this list will be used by the simulator.
8. **Populations** – When the region has been loaded, all the populations defined for the region will be listed here.

   When a population is selected, the initial counts for age or length groups in each population zone are displayed in
   the table, Item 9.

   These are used to initialize the simulator.
9. **Initial population** – This table holds the initial counts for each age or length group in each zone for the
   population selected.
10. **Number of months** – This specifies the period over which the simulation is carried out.
11. **Simulate** – Runs the simulation as specified by **Simulation launcher**, Item 21.
12. **Use pre simulation script** – Activates, or deactivates, the **Pre simulation script** tab and the script.

   Check the box to specify a pre-simulation script. The **Pre simulation script** tab will be activated and selected for
   editing the script (see below).

   To deactivate the pre-simulation script, uncheck the box. The **Pre simulation script** tab will also be deactivated.
13. **Use simulation plan** – Activates, or deactivates, the **Simulation plan** tab and the plans.

   Check the box to specify one or more simulation plans. The **Simulation plan** tab will be activated and selected for
   selecting and parameterizing the simulation plans (see below).

   To deactivate the simulation plans, uncheck the box. The **Simulation plan** tab will also be deactivated.
14. **Use Optimization method** – Activates, or deactivates, the **Optimization** tab and the optimization method.

   Check the box to specify that optimization is required. The **Optimization** tab will be activated and selected for
   selecting the objective function, optimization method and parameters (see below).

   To deactivate optimization, uncheck the box. The **Optimization** tab will also be deactivated.
15. **Simulation launcher** – Specifies where the simulations will be run:
    1. **in subprocess** – the simulations will be run in subprocesses. If several simulations are run at the same time, each
      will run in its own subprocess and so, potentially, each in parallel on a separate core.
    2. **on Caparmor server** – the simulations will be run on the Ifremer Caparmor supercomputer.
      See Installing ISIS-Fish on Caparmor for details of the configuration required.
    3. **in current process** – the simulations will be run in the current process.
16. **Save simulation parameters** – The simulation parameters can be saved for a future simulation. This has the same
   effect as the menu Simulation / Save simulation parameters.
17. **Available rules** – This is a list of rules that have been defined in ISIS-Fish.

   Each rule is followed by its description.
18. **Selected rules** – This is the list of rules that have been selected for the simulation.

   Each rule is followed by its description.

   When a rule is selected in this list, the parameters are displayed in the table below.
19. **Add** – Adds the rules selected in the Available rules, Item 10, to the Selected rules, Item 11.

   As each rule is added, a dialog box is displayed for setting the parameters.
20. **Remove** – Removes the rules selected from the Selected rules, Item 11.
21. **Clear** – Removes all the rules from the Selected rules, Item 11.
22. **Rule parameters** – When a rule in the Selected rules list is selected, the parameters are displayed in this table
   and may be modified.

   <img src="tip.png" /> When you move the pointer over a parameter name, a tooltip will appear with the description of the parameter.



### Pre simulation script tab

<img src="simulation_03_tabPreSimulScript.png" />

1. **Pre-simulation script editor** – For entering and editing the pre-simulation script.
2. **Back to parameter tab** – To return to the **Parameters** tab once the pre-simulation script has been defined.

### Simulation plan tab

<img src="simulation_04_tabSimulationPlan.png" />

1. **Dropdown list of simulation plans** – A list of all the simulation plans defined for ISIS-Fish.

   The plan filename is followed by its description.
2. **List of plans added** – The list of all simulation plans added to the simulation. When a plan is selected in this
   list, the parameters are displayed in the table, Item 6.

   All plans in this list will be used in the simulation.

   |tip| When you move the pointer over a simulation plan, a tooltip will appear with the description of the plan.
3. **Add** – Adds the plan selected in the list, Item 1, to the list of plans added, Item 2.

   A dialog box is opened to set to parameters for the plan.
4. **Remove** – Removes the plan selected (if there is one) from the list of plans added, Item 2.
5. **Clear** – Removes all the plans (if there are any) from the list of plans added, Item 2.
6. **Parameters for the selected plan** – When a plan is selected in the list of plans added, Item 2, the parameters are
   displayed in this table and may be modified.

   |tip| When you move the pointer over a parameter name, a tooltip will appear with the description of the parameter.
7. **Back to parameter tab** – To return to the **Parameters** tab once the simulation plans have been defined.



### Optimization tab

<img src="simulation_08_tabOptimization.png" />

1. **Objective function** – A dropdown list of all the objective function scripts defined.
2. **Objective function parameters** – This table displays all the parameters for the objective function. The values may
   be modified in this table.
3. **Optimization method** – A dropdown list of all the optimization scripts defined.
4. **Optimization method parameters** – This table displays all the parameters for the optimization script. The values
   may be modified in this table.
5. **List of exportable results** – A list of the exportable results that may be used in the calculation of the
   objective function.
6. **Add** – Adds the export selected in the list, Item 5, to the table of exports and observations, Item 9.
7. **Remove** – Removes the export selected (if there is one) from the table of exports and observations, Item 9.
8. **Clear** – Removes all the exports (if there are any) from the table of exports and observations, Item 9.
9. **Exports and observations** – This table lists all the exported values selected for optimization in the left hand
   column. The corresponding sets of observations can be selected in the right hand column.



### Results export tab

<img src="simulation_05_tabExportResult.png" />

1. **List of exportable results** – The results to be exported at the end of the simulation should be selected from this
   list of all export scripts defined.
2. **Save for next simulation** – saves the selections so that the next time the simulation launcher is run, the
   selected exports will be selected again.


### Result choice tab

<img src="simulation_06_tabResultChoice.png" />

1. **List of results available** – The results required from the simulation should be selected from this list of all
   results scripts defined.
2. **Delete results after exports execution** – If this box is checked, the results will be deleted after the exports
   scripts have executed.
3. **Save for next simulation** – saves the selections so that the next time the simulation launcher is run, the
   selected results will be selected again.



### Advanced parameters tab

<img src="simulation_07_tabAdvancedParams.png" />

1. **Simulator to use** – This sets the basic simulator configuration.

   1. Dropdown list of simulators available.
   2. **Simulation statistics** – Check the box to generate simulation statistics.
   3. **Simulation cache** – Check the box to use a simulation cache.
2. **Log filter settings** – Sets the log levels for deferent parts of ISIS-Fish.

   1. **Simulator log level** – sets the log level for the simulator itself.
   2. **Script log level** – sets the log level for scripts
   3. **Library log level** – sets the log level all other parts of ISIS-Fish
3. **Free parameters** – The top part of the Free parameters frame is used to define new free parameters. After setting
   the Tag and the Value, click Add to add the parameter to the list, Item 4.
4**Free parameter list** – This lists all the free parameters that have been defined
5. **Remove** – Removes the free parameter selected (if one is selected) from the list.
6. **Save for next simulation** – saves the settings so that the next time the simulation launcher is run, the settings
   will be reloaded.