# Introduction

## Purpose

Most fisheries are complex systems given the diversity of the resources exploited (many different species) and the
diversity of the fishing activities (many different types of fishing gear). The complexity of these mixed fisheries is
increased by the spatial and seasonal variations in both the resources exploited and the fishing activities. In these
mixed fisheries the resources are either exploited simultaneously or sequentially by fishing units (boat and crew) with
different fishing activities. It is difficult to evaluate the fish and fishing dynamics and the fishing mortality of the
populations exploited. The diversity of the fishing activities and their associated catches arises from the multiplicity
of fishing areas, target species and fishing gear as well as individual fishing strategies and economic and
environmental conditions.

Fishermen are aware of the spatio-temporal distributions of the resources exploited and allocate their fishing efforts
accordingly. At large scale the variations in the spatial distributions are mainly due to ontogenetic migration and
population movements, creating concentrations of certain developmental stages in particular areas in particular seasons
depending on certain life cycle processes such as reproduction and feeding.

The spatial and seasonal allocation of the fishing effort between the various fishing zones must be taken into account
when assessing fishery dynamics. For mixed fisheries this is even more important as fleets may not only change zone, but
also the fishing gear and target species. Spatially explicit models are required for understanding the dynamics of these
fisheries.

ISIS-Fish is a seasonal, spatial simulation model describing the dynamics of fishery resources, exploitation and
management. It was developed to investigate the effects of combinations of fishery management measures on fishery
dynamics. It can be used to compare the effects of conventional management measures such as total allowable catch (TAC),
fishing effort management, fishing gear restrictions and spatial management measures such as marine protected areas
(MPA).

ISIS-Fish has been designed to be as generic as possible so that it can be applied to different types of fishery. It
includes a database that holds a knowledge base for each fishery that can be easily updated. This knowledge base
includes the parameters describing each population and each fishing activity.

ISIS-Fish is very flexible to allow several hypotheses to be tested, in particular the relationships between the stock
of reproductives and reproduction, selectivity functions for fishing gear, etc., making it suitable for modeling a wide
range of pelagic, benthic and demersal fisheries. Functions describing fishery management measures and the response of
fishermen to these measures and to environmental and economic conditions can be coded using an interactive script
editor.

A number of French and European projects are currently developing ISIS-Fish models for various European fisheries.

## Look before you leap

 * Despite all the effort we have made to make the ISIS-Fish GUI easy-to-use and intuitive, there is a complex model
   hiding behind the GUI. Before you attempt to modify the model parameters, you should study and understand the equations
   and underlying hypotheses. Do not hesitate to use the mailing list to ask the ISIS-Fish community for help.
 * Java is required for setting certain ISIS-Fish parameters as well as for creating rules and simulation plans. A basic
   understanding of Java is essential for using ISIS-Fish effectively. We have written ISIS-Fish
   [simulation plan tutorials](../tutorials/simulationPlan.html>) and Oracle provides a comprehensive set of
   [Java tutorials](https://docs.oracle.com/javase/tutorial/).
 * The methods for the various classes used by ISIS-Fish are documented in
   [Java javadoc](https://docs.oracle.com/en/java/javase/17/docs/api/index.html) and
   [ISIS-Fish javadoc](http://api.isis-fish.org//index.html). You should familiarize yourself with Javadoc
   before using ISIS-Fish.
 * ISIS-Fish is a community project and we would like you to join this community by actively participating in the
   mailing list with questions, thoughts and scripts for the benefit of all users.
 * Finally, BEWARE, new users should take account of the above and the complexity of the ISIS-Fish model – setting it up
   for a particular application takes a long time and this step should not be underestimated.

## Overview of the model

ISIS-Fish is designed to evaluate the effectiveness of seasonal, localized management measures for mixed fisheries.
These measures control certain fishing parameters, in particular catches and fishing effort.

The simulator is based on three sub-models: a population dynamics model, a fishing activity model and a fishery
management model. Each of these sub-models is seasonally and spatially explicit.

<img src="modelesEn.png" />

The fishery limits are defined by a boundary line and the fishing area is divided into a regular grid of cells. The
resolution of the grid is selected depending on the output required and the data available. Zones, sets of contiguous
cells, are then defined independently for each population, each fishing activity and each management measure.

The simulation uses a monthly time step. Seasons, groups of months, are also defined independently for each population,
each fishing activity and each management measure. Within each combination of zone and season, the effort for a
particular fishing activity and the abundance of a population are assumed to be uniformly distributed.