# API documentation

* [Data model](isisFishModel.html)
* [ISIS-Fish Javadoc](http://api.isis-fish.org/)
* [ISIS-Fish source code](https://gitlab.nuiton.org/ifremer/isis-fish/tree/master/src/main/java/fr/ifremer/isisfish)
* [JAVA Javadoc](https://docs.oracle.com/en/java/javase/17/docs/api/index.html)

## Libraries

* [ToPIA Javadoc](http://nuiton.page.nuiton.org/topia-2.x/topia-persistence/apidocs/) (database interface)
* [Nuiton-Matrix Javadoc](http://nuiton.page.nuiton.org/nuiton-matrix/nuiton-matrix/apidocs/) (in particular the [MatrixND](http://nuiton.page.nuiton.org/nuiton-matrix/nuiton-matrix/apidocs/org/nuiton/math/matrix/MatrixND.html)  interface)
* [Nuiton-Utils Javadoc](http://nuiton.page.nuiton.org/nuiton-utils/apidocs/) (collection of methods for common operations)
* [SSJ Javadoc](http://umontreal-simul.github.io/ssj/docs/master/namespaces.html) (stochastic simulation library)
* [Nuiton-j2r Javadoc](http://nuiton.page.nuiton.org/nuiton-j2r/apidocs/) (interface between Java and R)
* [JDistLib Javadoc](https://jdistlib.sourceforge.net/javadoc/) (Java Statistical Distribution Library ([jdistlib](https://jdistlib.sourceforge.net/)))
