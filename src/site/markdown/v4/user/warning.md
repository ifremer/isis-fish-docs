# Disclaimer

We cannot guarantee that the results from ISIS-Fish are relevant for any particular purpose as they are dependent on the
data provided and the models selected. Users must satisfy themselves that the parameters used for an ISIS-Fish
simulation correspond to the known functioning of the fishery. The same is true for the results from a sensitivity
analysis carried out using the sensitivity analysis interface: the analysis method should be carefully selected. Not
every method can be used for any type of analysis and each type of analysis answers a predefined, specific, question.
