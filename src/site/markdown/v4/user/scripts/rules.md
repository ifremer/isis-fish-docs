# Rules

A rule is used to retrieve and set variables before and after each timestep of a simulation.

**NB all modifications to the database made by a rule are temporary and persist only to the end of the timestep.**

## Rule scripts

Rules are stored in the directory `rule` and are classes that extend `fr.ifremer.isisfish.rule.AbstractRule` which
implements the interface `fr.ifremer.isisfish.rule.Rule`.

Several methods are used by ISIS-Fish.

* `getDescription()` – Returns a String describing the rule.
* `getNecessaryResult()` – Returns a string array with the results required for the rule.
* `init()` – Called once only at the start of the process.
* `condition()` – Called before preAction() to determine whether the rule should be applied to the next timestep.
* `preAction()` – Called **before** each timestep.
* `postAction()` – Called **after** each timestep.

## Method call sequence

The methods are called by the simulator.

At the start of the simulation process, ISIS-Fish instantiates each of the rules selected for the simulation and calls
`init()` for each rule. The `init()` method is, therefore called only once.

Then for each timestep the simulator

* calls `condition()` for all metiers and all rules (the `condition()` methods for all rules are called before any other methods for any rule);
* calls `preAction()` for each rule and each metier provided that the `condition()` method returned true for the rule and the metier;
* simulates the timestep;
* calls `postAction()` for each rule and each metier provided that the `condition()` method returned true for the rule and the metier;
* rolls back all changes made by the rules.

```

  // debut de simulation
  pour toutes les règles
    appel de la méthode init() de la règle
  finpour

  pour chaque pas de temps
    // condition
    pour toutes les regles
      pour tous les metiers
        appel de la methode condition()

    // preAction
    pour toutes les regles
      pour tous les metiers
        si la regle est active pour ce metier
          appel de la methode preAction()

    // simulation du pas de temps

    // postAction
    pour toutes les regles
      pour tous les metiers
        si la regle est active pour ce metier
          appel de la méthode postAction()
```

## Care required

### preAction / postAction

The `preAction()` and `postAction()` methods are called several times for each timestep (once for each metier). If
`preAction()` and `postAction()` should only act once per timestep, a trick, such as a state field, must be used to
suppress the repeated actions.

### Field initialization

Rules are only instantiated once and `init()` is called only once. The fields are, therefore, not reinitialized
between timesteps.

### Persistence of the modifications

The modifications made by a rule are rolled back at the end of each timestep. Persistent changes made to the database
must be repeated for each timestep.


## Links

  * http://api.isis-fish.org//fr/ifremer/isisfish/rule/Rule.html
  * http://api.isis-fish.org//fr/ifremer/isisfish/rule/AbstractRule.html
  * http://svn.forge.codelutin.com/svn/isis-fish-data/trunk/rules
  * http://svn.forge.codelutin.com/svn/isis-fish-community/trunk/rules/
