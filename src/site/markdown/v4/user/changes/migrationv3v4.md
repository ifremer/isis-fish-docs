# Migration from V3 to V4

One of the side effects of the changes between V3 and V4 is that the data model has been changed and so V3 databases
cannot be migrated to V4 automatically. Manual intervention is required.

## Major changes

### `Date` renamed as `TimeStep`

The IsisFish `Date` class conflicted with the standard Java `Date` class and has been renamed `TimeStep`. The new
`TimeStep` class has the same content and behavior as the old `Date` class.

### `AnalyzePlan` renamed as `SimulationPlan`

To avoid confusion between analysis plans and sensitivity analysis plans, analysis plans are now called simulation plans
and so the `AnalyzePlan` interface has been renamed `SimulationPlan`. `SensitivityCalulator` has been renamed as
`SensitivityAnalysis`.

### Maturity groups removed

Maturity groups, which were defined as groups within a population, have been removed and replaced by a “Maturity ogive”
equation.

### Reproduction rate moved

The reproduction rate field for a population group has been removed and replaced by an equation for the whole
population.

## Database migration

Fisheries can be migrated from V3 to V4 by exporting them from ISIS-Fish V3 and importing them into ISIS-Fish V4 (V3 and
V4 can be executed at the same time).

When importing, a warning pop-up asks you to confirm the operation.

<img src="migrationv3v4/migrationdb.png" />

If there are any equations that fail to compile during the import, the import assistant pops up the equation editor for
manual correction.

For example, the following equation uses the obsolete `Date` class which should be changed to `TimeStep`.

<img src="migrationv3v4/migrationequation.png" />

## User script migration

Subdirectories with user scripts in the `isis-database-3` directory must be copied to the
`isis-fish-4/isis-community-database` directory.

Restart ISIS-Fish and select the **Scripts** window. The scripts that have been copied should appear in the
**Community VCS** branch of the script tree.

The menu **Code / Check all scripts** can then be used to recompile all the scripts to check for errors.

Typical errors are :

cannot find symbol: class Date
> Replace `Date` by `TimeStep`

cannot find symbol: class AnalyzePlanIndependent
> Replace ``AnalyzePlanIndependent`` by ``SimulationPlanIndependent``

cannot find symbol: class AnalyzePlan
> Replace `AnalyzePlan` by `SimulationPlan`

warning: [deprecation] getMonth() in java.util.Date has been deprecated
> Will disappear when `Date` is replaced by `TimeStep`

cannot find symbol: method next()
> Will disappear when `Date` is replaced by `TimeStep`

cannot find symbol: method setReproductionRate(double)
> `ReproductionRate` is now an equation associated with a population

cannot find symbol: method setMaturityGroup(PopulationGroup)
> This method has been removed from ``PopulationGroup`` as the maturity is now defined by a “Maturity ogive” equation
> for the whole population

N.B. The obsolete package declarations "package analyzeplan;" and "package sensitivitycalculator;" for simulation plans
and sensitivity analyses do not raise compilation errors so be sure to change them.

## Rule migration

Rules that manipulated the maturity group or reproduction rate parameters will need to be updated as these parameters
are now equations associated with the population and not population groups.

The values of these parameters can still be recovered using a group object, e.g. `group.getMaturityOgive();`

However, the population must be used to set a value, e.g. `pop.setMaturityOgiveEquation(equation);`
