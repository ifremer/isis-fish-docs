<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# New features of V4.4

This page lists the major changes in version 4.4

## Recruitment

Before V4.4 users had no control over recruitment and there was no explicit relationship between stocks and recruitment.
Recruitment could be simulated by adjusting the reproduction equation.

V4.4 introduced an explicit recruitment equation, defined in the **Recruitment** tab for the population, together with
the reproduction equation.

The reproduction equation is used to calculate the number of eggs laid in each reproduction zone in each month.

If the default recruitment equation is used (`return 0;`) recruitment process is the same as in earlier versions:

* “null” group mortality for the eggs from spawning to recruitment,
* recruitment in the recruitment zones defined by the correspondence table between spawning zones and recruitment zones
  (**Zones** tab for the population) …
* … in the months defined by the recruitment delay and distribution.

The recruitment equation can be used to either

* change the default recruitment behavior or
* define the recruitment without defining the number of eggs laid (using the default reproduction equation:
  `return 0;`).


The complete signature for the compute method is
```java
  public double compute(SimulationContext context, TimeStep step,
      Population pop, ReproductionDataMap reproductions, MatrixND result) throws Exception {
  }
```

This is called to modify the result matrix using the other parameters. It is called for each time step within the
recruitment period (spawning month + recruitment delay + recruitment distribution).

N.B. the `result` matrix is not empty when the method is called. It already holds the default recruitment numbers
(see [PopulationMonitor#getRecruitment](https://gitlab.nuiton.org/ifremer/isis-fish/-/blob/master/src/main/java/fr/ifremer/isisfish/simulator/PopulationMonitor.java#L252)
for details). This means that the `result` matrix is valid even if the recruitment equation is empty.

### Recruitment equation parameters

context: [SimulationContext](https://gitlab.nuiton.org/ifremer/isis-fish/-/blob/master/src/main/java/fr/ifremer/isisfish/simulator/SimulationContext.java)

  Simulation context

pop: [Population](http://api.isis-fish.org/index.html?fr/ifremer/isisfish/entities/Population.html)

  Population

reproductions: [ReproductionData](http://api.isis-fish.org/index.html?fr/ifremer/isisfish/types/ReproductionData.html)

  This data map is specific to the recruitment equation. It holds values of certain parameters, particularly the
  reproduction, for each month of the reproduction period (0 for the first month, 1 for the second month …)

  The map holds a recruitmentInput object.

recruitmentInput: [RecruitmentInput](http://api.isis-fish.org/index.html?fr/ifremer/isisfish/types/RecruitmentInput.html)

  Contains the abundance, biomass and the value returned from the reproduction equation for this time step.

result: [MatrixND](http://nuiton.page.nuiton.org/nuiton-matrix/nuiton-matrix/apidocs/org/nuiton/math/matrix/MatrixND.html)

  The default recruitment (reproduction * mortality * migration from the spawning areas * temporal distribution).

For example, if the spawning season extends from January to March, the delay from spawning to the start of recruitment
is 8 months and the recruitment (p  matEtalement) is spread over two months (with a 0.7 of the recruitment the first month and 0.3 the
second), then `recruitement` will hold

  * In September using reproduction in
    - January (index 0) : ReproductionData (with reproduction data for January, p=0.7)

  * In October adding reproduction in
    - January (index 0): ReproductionData (with reproduction data for January, p=0.3)
    - February (index 1): ReproductionData (with reproduction data for February, p=0.7)

  * In November adding reproduction in
    - February (index 1): ReproductionData (with reproduction data for February, p=0.3)
    - March (index 2): ReproductionData (with reproduction data for March, p=0.7)

  * In December using reproduction in
    - March (index 2): ReproductionData (with reproduction data for March, p=0.3)

Sample recruitment scripts can be found on the [shared scripts](../scripts.html) page.

### Calls from the simulator

The default simulator calls the recruitment equation after the calculation of the population migration. You can,
therefore, check the time when it is called.


## Results

The definition of the results has been modified to make it easier to add results without modifying `ResultName.java`.

The results are now defined by scripts in the `resultinfos` directory. Each result is identified in the script by
setting the static field `NAME` to the name that was previously declared in the `ResultName.java` file.

For example, the MatrixAbundance result is defined by
```java
  public class MatrixAbundance extends AbstractResultInfo {

    public static final String NAME = MatrixAbundance.class.getSimpleName();

    @Override
    public String getDescription() {
        return "do the doc of Result MatrixAbundance";
    }
  }
```

`MatrixAbundance.class.getSimpleName()` returns the name of the class, in this case the string `"MatrixAbundance"`,
normally the name of the script should not be modified.

Old scripts that still use `ResultName.MatrixAbundance` will still work but ISIS-Fish will raise a warning that the
code is deprecated and should be updated to `MatrixAbundance.Name` to work with future versions.


## Export

In previous versions exports were executed at the end of the simulation and if this failed nothing was exported. Now,
the exports are executed at each time step.

In ISIS-Fish 4.3 exports were implemented using the `Export` interface.
```java
  public class Abundances implements Export {

    public void export(SimulationStorage simulation, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(pop, MatrixAbundance.NAME);
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                TimeStep step = (TimeStep) sems[0];
                PopulationGroup group = (PopulationGroup) sems[1];
                Zone zone = (Zone) sems[2];

                double val = i.getValue();
                out.write(pop.getName() + ";" + group.getId() + ";" + zone.getName() + ";" + step.getStep() + ";" + val + "\n");
            }
        }
    }
  }
```

The `export` method is called at the end of the simulation.

This approach can still be used in version 4.4 but using the new `ExportStep` interface is safer.
```java
  public class Abundances implements ExportStep {

    @Override
    public void export(SimulationStorage simulation, TimeStep step, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(step, pop, MatrixAbundance.NAME);
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                PopulationGroup group = (PopulationGroup) sems[1];
                Zone zone = (Zone) sems[2];

                double val = i.getValue();
                out.write(pop.getName() + ";" + group.getId() + ";" + zone.getName() + ";" + step.getStep() + ";" + val + "\n");
            }
        }
    }

    @Override
    public void exportEnd(SimulationStorage simulation, Writer out) throws Exception {

    }
  }
```

Classes implementing the `ExportStep` interface must have two methods:
* `export` – called after each time step
* `exportEnd` – called at the end of the simulation

## Export scripts

Official export scripts have been migrated to V4.4 and now export after each time step.

This migration was also to opportunity to

* move `step` to the first column
* add a header to the csv file

The scripts in the community depository have not been migrated but should still work.

## Fishing mortality due to other fleets

The **Fishing mortality other fleets** equation has been added to the **Stocks** tab for a population. This specifies
the fishing mortality due to fleets that are not modeled explicitly. The structure is identical the natural death rate
equation (death rate per group and, possibly, per zone). The equation is called each time the natural death rate
equation is called and the mortalities are summed. The fishing mortality calculated by ISIS-Fish includes only the
catch by the fleets modeled explicitly, but it is affected by the fishing mortality due to other fleets in each zone.

## Bug fixes

And loads of bug fixes :)

For a complete list, see : https://forge.codelutin.com/versions/645.
