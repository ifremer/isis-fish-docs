# V4.0 Changes

This page lists the major changes introduced in V4.0

## New equations

New equations have been introduced to improve the definition of a fishery.

* Maturity ogive (a population property that replaces the maturity group concept in V3)
* Reproduction (a population property that replaces the group reproduction rate)
* Accessibility (this equation may be used in place of the accessibility table)

## Group screening

Group screening was introduced to modify a group of factors together for sensitivity analysis

## Factors

More fishery parameters can be defined as factors for analysis.

* Using rules as factors
* Using rule parameters as factors
* Defining a set of equations as a discrete valued factor
* Defining the initial populations as factors

## Community repository

There is now a community repository for user scripts as well as the official repository

This repository holds scripts written and submitted by users. Authenticated users can submit any scripts that may be of
interest to other users and any user can download these scripts, simplifying collaboration between users.

## Renamed classes and interfaces

Some classes have been renamed for clarity

* Date class has been renamed TimeStep to avoid confusion with java.util.Date
* AnalysePlan interface has been renamed SimulationPlan to avoid confusion with sensitivity analyses
* SensitivityCalculator interface has been renamed SensitivityAnalysis

## Bug fixes

And loads of bug fixes :)

For a complete list, see https://forge.codelutin.com/versions/85
