# V4.4.1 Changes

This page lists the major changes in V4.4.1 

## Spatial adjustments

V4.4.1 adds a new interface for adjusting the spatial resolution and limits of a region.

It is accessed using the menu Region / Change the spatial resolution

<img src="changements441/newresolution.png" />

When the spatial proprieties have been adjusted, a pop-up window shows the old and new regions side by side. Zones (or
ports) can be selected from the drop-down list and the right hand pane shows the cells selected for the recalculated
zone or port. The selection can be adjusted before the new spatial definition is saved.

<img src="changements441/spatialpreview.png" />

## JSON import / export

A JSON import / export function has been added to
* export objects from a region and import them into a different region
* copy objects within a region

### JSON export

The menu item **Region / Export the current object in JSON** pops up a file export menu to export the currently selected
branch of the object tree to a zip file *myfilename*.json.gz

### JSON import

The menu item **Region / Import objects from JSON** pops up a file import menu to import *myfilename*.json.gz into the
current region.

If the import process detects a conflict or ambiguity for an object, a dialog box requests the user to select an action
for this, or all objects (of the same type).

The options are

* **None** – Do nothing for this object
* **Use** – Use an object already defined for the region rather than creating a new object
* **Replace** – Replace the object in in the database by the imported object
* **Create** – Create a new object in the database
* **Abort** – Abandon the import

The user can request that the option selected be applied to all objects, or to all objects of the same type. For
example, when importing a zone, the user can

* select **Create** for the zone object
* Select **Use** for the first cell and all the other cells in the database

In this case, only the zone object will be created, using the cells of the same name already defined for the current
region.

## Recruitment equation

The functioning of the recruitment equation has been modified to improve its applicability.

In particular, for populations defined in terms of age groups, the fish are recruited into the age groups corresponding
to the age at recruitment, taking the number of months after spawning and the month for changing class into account.

## Database change log

A CSV log file is now created in the simulation directory. It lists all the modifications made to the database, sorted
by type.

## Bug fixes

And loads of bug fixes :)

For a complete list, see https://forge.codelutin.com/versions/1020
