# Running simulations from R

## With local region

Since version 4.4.2.1, ISIS-Fish now includes a new command line action to run simulation with existing region and
a random prescript file.

Here is the command line that can be used from R:
```shell
  java -jar target/isis-fish-4.4.x.y.jar --simulationWithRegionNameAndScript simulationID regionName prescriptPath
```

With:

* simulationID : must be a unique simulation name
* regionName: name a already existing region located in `isis-fish-4/isis-database/region`
* prescriptPath: path to prescript file


## Prescript example in sensitivity analysis context

In the prescript file, the `context` object (`SimulationContext`) can be used to modify simulation before execution.

### Simulation parameters

Keep in mind that, using `simulationWithRegionNameAndScript` action, simulation parameters are **totally empty**
(no population, no strategie, no effective...), so your prescript will have to valuate them.

But your can restore a default "parameters.properties" to avoid a lot of coding using
```java
  SimulationParameter params = context.getSimulationStorage().getParameter();
  Properties props = new Properties();
  props.load(new FileReader(new File("/path/to/simulation/parameters.properties")));
  params.setProperties(props);
```

### Manipulation region objects

Using `context` and database DAO (data access object), you can manipulate all your region objects. For example
```java
  GearDAO dao = context.getGearDAO();
  Gear engin = dao.findByName("Senne");
  engin.setStandardisationFactor(0.01);
```

### Using matrix

You also can import or use `Matrix` to valuate your region's objects. For example, to change the capturability matrix
of a specific population:
```java
  PopulationDAO pDao = context.getPopulationDAO();
  Population pop = pDao.findByName("Anchois_long");
  MatrixND mCap = pop.getCapturability();
  pop.setCapturabilityEquationUsed(false);
  mCap.importCSV(new FileReader("/path/to/csv/file.csv"), new int[] {0, 0});
```

### Using simulation name

In case you need to use a dynamic parameter to generate or read external file in prescript depending of a specific simulation
execution, you can use the simulation name:
```java
  String simulationName = context.getSimulationStorage().getName();
```
