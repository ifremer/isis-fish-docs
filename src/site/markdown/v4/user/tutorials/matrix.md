# Using matrices

Matrices, tables and arrays are handled by an external library [Nuiton-matrix](http://nuiton.page.nuiton.org/nuiton-matrix/nuiton-matrix/apidocs/).

## Importing N dimensional matrices with semantics

### File format

A special version of the sparse matrix CSV format is used to import matrices, complete with their semantics.:
```
  [dimension,dimension...] DefaultValue
  class:instance,instance...
  class:instance,instance...
  ...
  index1;...indexN;Value
  index1;...indexN;Value
  ...
```

For example:
```
  [2,3] 0
  Population:merluccius,nephrops
  Zone:zone_hake1,zone_hake2,zone_langust
  0;0;42
  1;2;24
  1;0;0.5
```

This file defines three non-zero values for (merluccius,zone_hake1), (nephrops,zone_hake2) and (nephrops,zone_hake1) all
the other cells having the default value (0).

### Class and instances

The class and instances must correspond to existing objects.

The types may be Java classes

* ``java.lang.String``
* ``java.lang.Integer``
* ``java.lang.Double``
* ...

or ISIS-Fish classes

* ``Cell``
* ``Zone``
* ``Port``
* ``Species``
* ``Population``
* ``PopulationGroup``
* ``Gear``
* ``VesselType``
* ``TripType``
* ``SetOfVessels``
* ``Strategie``
* ``Observation``
* ``Metier``
* ...

Each instance must be identified by its name. For example, the populations “merluccius” and nephrops should be declared
as `Population:merluccius,nephrops`.

However, age and size groups do not have a name – they are identified by number (starting at 0). To identify a group,
the population name must be suffixed by “+” and the group number: `PopulationName+GroupNumber`, e.g.:
```
  PopulationGroup:nephrops+0,nephrops+1,nephrops+2,nephrops+3,nephrops+4,nephrops+5,nephrops+6
```

Fo the same reason, to identify a population season,
the population name must be suffixed by “+”, the season first month, another "+" and the season last month: `PopulationName+firstMonth+lastMonth`, e.g.:
```
  PopulationSeasonInfo:nephrops+0+0,nephrops+1+10,nephrops+11+11
```
