# Training

* Java base : [1_Isis_Java_base.pdf](http://data.isis-fish.org/isis-fish/formations/1_Isis_Java_base.pdf)
* Java objects : [2_Isis_Java_objet.pdf](http://data.isis-fish.org/isis-fish/formations/2_Isis_Java_objet.pdf)
* Java and ISIS-Fish : [3_Isis_Java_In_Isis.pdf](http://data.isis-fish.org/isis-fish/formations/3_Isis_Java_In_Isis.pdf)
* Java and R : [4_Isis_Java_In_R.pdf](http://data.isis-fish.org/isis-fish/formations/4_Isis_Java_In_R.pdf)
