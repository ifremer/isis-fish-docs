# Useful ssh commands on Datarmor or other supercomputers

When you run simulations on Datarmor, all the simulation files will normally be stored in isis-tmp in your home
directory (see [Datarmor configuration](../installation.html)).

## Look at the job queue
```shell
  $ qstat
```

## Look at the status of a job
---------------------------
```shell
  $ qstat –f JobID
```
For example:
```shell
  $ qstat –f 1234567[].service0
```

## Read the simulation log

If a simulation fails, then the log will be found in the file simulation-sim…-output.txt. To read this file:
```shell
  $ cat simulation-sim…-output.txt
```

### Rerun simulations within a simulation plan

If certain simulations (for example simulations 5 to 10) within a plan need to be re-run, they can be submitted
directly:
```shell
  $ qsub –J 5-10 simulation-sim…-script.seq
```

### Recovering results that have not been returned

If the simulation has completed correctly but the results have not been returned, the simulation file,
simulation-sim...-result.zip, can be recovered by copying it to the transfer server.
```shell
  $ cp *-result.zip /home/navidad/partages/echange/Fred
```

### Unix commands

For an overview of Unix (Linux) commands see https://www.tutorialspoint.com/unix/unix-useful-commands.htm
