# Using the API documentation

All values entered into the user interface is stored in ISIS-Fish objects. If the user wishes to use a script to modify
these values before or during a simulation, it is necessary to know the type and structure of the classes and how to
locate the values. To locate the values, it is necessary to know which classes are attributes of other classes: this
information can be found in the [ISIS-Fish UML Class diagram](../isisFishModel.html) showing the internal architecture which is mirrored in
the user interface.

To navigate from one object to another, it is necessary to understand the methods associated with the classes. The API
documentation provides all you need to know.

Note. It is probably best to start off by familiarizing yourself with basic Java classes (Double, Boolean, String,
Integer) and their attributes and methods (see Oracle Java tutorials and Java for beginners).

This tutorial gives a step by step description of the process for finding out how to modify the natural death rate
formula in a simulation plan. The script will replace the predefined formula by new formula held in a string.:
```java
  String NewNaturalDeathRate = "if (groupe.getId() == 0) return 1.5; else return 0.2;"
```

## Question: “How to find the natural death rate in the data structure”

You can find it in ISIS-Fish UML Class diagram but you can also find it using the user interface – the natural death
rate is set in the **Life History Traits** tab for the population branch of the object tree.

In plain English we might say “find the natural death rate in the population and set the new value”. We just have to say
it in Java.

## Go to the API documentation page

Find “population” in the API index on the left hand side of the window and click it to display the “population”
interface in the main window.

## The methods

Scroll down in the main window to the Method Summary. The right hand column lists all the methods that can be used with
a population object and the left hand column has the corresponding class of object or type of variable that is returned
by each method.

These could be used in assignments of the type:
```
  variable_of_type_in_left_column = population.method_in_right_column(arguments);
```

There are two very common types of method:

* “get” methods, which retrieve something from an object
* “set” methods, which set something in an object

To set the natural death rate a “set” method would be the natural choice.

## We must look for methods for the natural death rate

The list of methods includes `getNaturalDeathRate()` which returns a reference to an “equation” object (the value
that was set by the user as an equation in the **Life History Traits** tab) and
`setNaturalDeathRate(Equation naturalDeathRate)` to which we must pass the formula for the natural death rate as an
Equation object.

It would, therefore seem that `setNaturalDeathRate()` is just what we need, but, unfortunately,
``setNaturalDeathRate()`` takes an Equation object as the argument and:
```java
  pop.setNaturalDeathRate(NewNaturalDeathRate);
```
will not work as ``NewNaturalDeathRate`` is a string.

## Modifying the existing Equation object

We must find another approach. If we click on Equation in the API index and scroll down to the Method Summary we find
the `setContent (String content)` method. This takes a `String` argument and we can make a wild guess that the
“content” is the formula itself. We need to recover a reference to the existing object and modify it directly.

We have already found the `getNaturalDeathRate()` which returns (a reference to) an Equation object.:
```
  Equation NaturalDeathRate = pop.getNaturalDeathRate();
```

And as `setContent` takes a string as an argument, we can then set the new natural death rate formula directly.:
```java
  naturalDeathRate.setContent(NewNaturalDeathRate);
```

This will update the natural death rate for the population.
This could be done in one statement.:
```java
  pop.getNaturalDeathRate().setContent(NewNaturalDeathRate);
```
