# Configuration R

To configure ISIS-Fish to work with R, you will need:
 - Java (x64)
 - ISIS-Fish
 - R (x64)

## Windows Environment variable

The first action is to define an environment variable by adding R bin directory to `Path` environment variable in windows:
 - copy R installation path (ex: `C:\Program Files\R\R-4.3.2\bin\x64`)
 - right-click on "This PC", open "Properties"
 - Open "Advanced System Settings"
 - Open "Environment variables..."
 - In bottom part "System variables", modify `Path` varaible by adding `C:\Program Files\R\R-4.3.2\bin\x64`

<img src="rconfigwindows.gif" />

## ISIS-Fish

Then, Open ISIS-Fish and use R configuration assistant
