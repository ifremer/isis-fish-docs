# Tutorials

* [Simulation plans](tutorials/simulationPlan.html)
* [Optimization scripts](tutorials/optimization.html)
* [Oracle Java tutorials](https://docs.oracle.com/javase/tutorial/)
* [Java for beginners](https://beginnersbook.com/java-tutorial-for-beginners-with-examples/)
* [Using the API documentation](tutorials/useAPI.html)
* [Some useful CAPARMOR commands](tutorials/utilCaparmor.html)
* [Using matrices](tutorials/matrix.html)
* [Running simulation from R](tutorials/simulationFromR.html)
* [Sensitivity analysis tutorial](../../downloads/isisSensitivity.pdf)

<!-- [Handbook for conducting a calibration of an ISIS-Fish model with the library R Calibrar](../../downloads/HANDBOOK_Calibrar_ISIS-Fish.pdf) -->
