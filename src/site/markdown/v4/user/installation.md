# ISIS-Fish installation and execution

## System configuration

Java Development Kit (JDK) version **8 (or newer)** must be installed on the machine for using ISIS-Fish, **java 17** is now the preferred version.
Typing `javac –version` at
a command prompt in either Windows or Unix/Linux will display the current JDK version number. If this command gives an
error message then JDK is not installed. If JDK is not installed or it is not version 8 or newer (build 1.8.xxxx), Java
Development Kit version **8 (or newer)** should be downloaded from the [Java Standard Edition download page](https://www.oracle.com/java/technologies/downloads/) and installed.


## Downloading ISIS-Fish

ISIS-Fish can be downloaded from the ISIS-Fish installation file repository. The archive file isis-fish-4.x.y.z.zip
(where 4.x.y.z is the version number) contains all the files required to use ISIS-Fish.

## Installation

The downloaded file should be unzipped into the required directory (folder). The Linux command:
```shell
  unzip isis-fish-4.x.y.z.zip
```
will unzip the files into the isis-fish-4.x.y.z directory

## Execution

### Standard files in the isis-fish-4.x.y.z directory

The standard command files, `isisfish.bat` for Windows and `isisfish.sh` for Linux, allocate a maximum of 2Gb memory
this can be changed by editing the `–Xmx` option for the java command in `isisfish.bat` or `isisfish.sh`.

By default the log is output to the file `debug.txt` in this directory. If there is a problem during an ISIS-Fish run,
this file should help to track it down. If you post a request for help on the mailing list, the `debug.txt` file
should be attached to the post.

### Running ISIS-Fish

Under Windows – Double click isisfish.bat. A DOS command widow will open, and then, after a small delay, the ISIS main
window will open.

Under Linux – Navigate to the isis-fish-4.x.y.z directory and type the command:
```shell
  ./isisfish.sh
```
<img src="mainWindow.png" />

If the ISIS main window does not open, then there must have been a startup error. Although the error message should be
in the file `debug.txt`, the message is not always easily comprehensible. If you do not understand the message, post
the file to the user mailing list ([Contact and Help](../../contact.html)).

## Initial data and scripts

The first time ISIS-Fish is executed, if the workstation is connected to the Internet, it will fetch a demonstration
region and scripts for simulation, export, etc. 

## Updating the scripts

Simulation script updates are independent of the ISIS-Fish version updates and the updated scripts may be used without
updating ISIS-Fish. In some cases, for example, if properties have been added to an object describing a region, it may
be necessary to install a new version of ISIS-Fish.

The scripts are updated from the main window using the **File / Server synchronization** menu.

The Synchronization window lists the any modified or new scripts on the server. You can select the scripts to be
downloaded and preview the modifications.

## Editing the isis-config-4 configuration file

When the default configuration has been changed, the changes are saved in the configuration file `isis-config-4`. This
file can be edited, copied and restored to allow several configurations to be used on the same machine. The
configuration file is saved in the (roaming) home directory (`$HOME/` for Linux, `Users/account/` for Mac or
``%userprofile%\AppData\Roaming\`` for windows).

By default, the working files are all in the Isis-Fish-4 directory in the user’s home directory (`$HOME/` for Linux,
`Users/account/` for Mac or `%userprofile%\` for windows).

Other directories can be used for these files by setting the pathnames in isis-config-4 file.:
```properties
 #Last saved Sat May 12 16:46:04 CEST 2018
 #Sat May 12 16:46:04 CEST 2018
 compilation.directory=D\:\\SimulationsISIS\\isis-build
 database.directory=D\:\\SimulationsISIS\\isis-database-4
 default.export.directory=D\:\\SimulationsISIS\\isis-export
```

Note that “:” and “\” must be preceded by “\”.

## Installing ISIS-Fish on the DATARMOR supercomputer

If the DATARMOR supercomputer is to be used for simulation, then ISIS-Fish must also be installed under your DATARMOR
account. The procedure Installing ISIS-Fish on DATARMOR describes the installation of ISIS-Fish on DATARMOR and the
configuration of your local copy of ISIS-Fish to use DATARMOR.

[DATARMOR ISIS installation](installation-isis-datarmor-en.pdf)
