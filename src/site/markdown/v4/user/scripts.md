# Shared scripts page

This page is a collection of ISIS-Fish users’ application specific scripts that the authors have been willing to share
with the community.

When submitting a script, please supply
* the author’s name;
* the version of ISIS-Fish;
* the region;
* for simulation plans, the purpose – exploration of a parameter space, sensitivity analysis, etc.;
* description of the function of the script.

## Simulation plans

### P_As_planFactFractV-12param for the Bay of Biscay anchovy fishery

Sigrid – V3.1.3 – Simulation plan for sensitivity analysis of the model of the Bay of Biscay anchovy fishery

Group screening for 12 groups of parameters (standardization factors, adult natural death rates, juvenile natural death
rates, accessibility, migration, length class limits, selectivity, target factors, inactivity, total authorized catch
(French and Spanish fleets), MPA parameters) with a fractional factorial design at resolution V (256 runs).

[Simulation plan, experimental design matrix and parameter files](../downloads/P_As_planFactFract_12param_Anchois.zip)

### Accessibility calibration

Sigrid – V3.3.0.8 – Simulation plan for calibrating accessibility

Using a simplex method (Walters, F.H., Parker, L.R., Morgan, S.L. and Deming, S.N. 1991. Sequential Simplex
optimization: a technique for improving quality and productivity in research, development, and manufacturing
(Chemometrics series). B.R. CRC Press LLC. 402 pp.) Five initial values are specified at equal intervals and the
algorithm finds the value that minimizes the objective function using smaller and smaller steps.

[Simulation plan](../downloads/P_Calibration_1param.zip)

### Simulation plan modifying fishery management rules

Sigrid – V3.3.0.8 – Simulation plan exploring the effect of changes in fishery management rules for the Bay of Biscay
anchovy fishery

[All files required for this plan](../downloads/PlanModifRules.zip)

### Simulation plan modifying fishery management rules and parameters

Sigrid – V3.3.0.8 – Simulation plan exploring the effect of changes in fishery management rules and inactivity
parameters for the Bay of Biscay anchovy fishery

[All files required for this plan](../downloads/PlanRulesParam.zip)

## Rules

Sigrid – V3.3.0.4 – Rule

Modification of the total authorized catch script to apply from July to June, to apply to only one strategy and to close
the fishery if the biomass falls below a threshold.

[Modified TAC script](../downloads/TACfix_Es_Anchois.java)

Sigrid – V3.3.0.8 – Rule

Reducing the fishing effort by changing the inactivity

[Fishing effort reduction](../downloads/EffortReduction.java)

Sigrid – V3.3.0.8 – Rule

The % of fishing effort for each metier for each month is defined by a file

[Effort %](../downloads/Effort_2000_2004_MultiSp.java)

Coming soon :

* A rule based on a Random Utility Model (RUM)
* A rule based on a RUM and TAC
* A Harvest Control Rule setting the TAC as a function of the biomass

## Prescript

Prescript allowing to replace the capturability parameters at the beginning of each simulation by values containing in a csv file. The path that leads to the csv file takes into account the name of the simulation in progress.

[Change capturability parameters](../downloads/ChangeCapturabilityParameters.java)


## Equations

Some tricky equations to give an idea of what is possible

* [Simple von Bertalanffy growth function (VBGF)](../downloads/VBGFAnchois.java)
* [Simple von Bertalanffy growth function (VBGF) as a function of the month](../downloads/VBGFinmonthSole.java)
* [Weight as a function of the month](../downloads/Weight_month.java)
* [Specialized natural death rate](../downloads/MzonesAnneeMpertMgdg.java) changing each year for the first group
* [Reproduction](../downloads/Repro_recru0-2_StockOeufs_Spatialisee061107.java) with fixed values for the first three years
* [Simple stock-recruitment equation](../downloads/StockRecru.java)
* [Equation modifying the default recruitment](../downloads/ModifRepro.java) by suppressing the associated migration
* [Density dependent migration](../downloads/MigrationAout.java)
* [Migration](../downloads/MigrationAdultsAvrilHistoric.java) coefficients set to observed values each year
* [Price as a function of landings](../downloads/prixAnchois.java)
