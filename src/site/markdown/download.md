<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# Download ISIS-Fish

## ISIS-Fish (Java version)

Only the Java version of ISIS-Fish is supported on this website. The current and previous versions are available here.

* [Download ISIS-Fish](https://forge.codelutin.com/projects/isis-fish/files)

## Regional models

### Bay of Biscay dataset

This is a demonstration dataset that is more advanced than the DemoRegion dataset. ISIS-Fish V3.3, or more recent, is
required.

* [Golfe de Gascogne dataset](downloads/GolfeDeGascogneDemoV3.3.0.3.zip)
* [Golfe de Gascogne workshop](http://data.isis-fish.org/isis-fish/Atelier.zip)

### Bay of Biscay scenario dataset

This is a dataset with simulations for the scenarios created by the “Dents du Golfe” (Jaws in the Bay) forecasting
workshop. ISIS-Fish V4.3, or more recent, is required.

* [Golfe de Gascogne Dents du golfe dataset](downloads/GdGMerluLangSole_21Mai2015.zip)
* [simulation Dents du golfe SQ](downloads/sim_sim_STATU QUO 50 ANS_cantonnement_2016-05-11-09-23.zip)
* [simulation Dents du golfe Sc1 12-18m](http://data.isis-fish.org/isis-fish/sim_Zoe_bateau1218m_sc1_2016-05-10-22-38.zip)
* [simulation Dents du golfe Sc2 sup18m](http://data.isis-fish.org/isis-fish/sim_Zoe_bateausup18m_sc2_2016-05-10-22-47.zip)

* [simulation Dents du golfe Sc3 inf12msup18m](http://data.isis-fish.org/isis-fish/sim_Zoe_bateauInf12msup18m_sc3_2016-05-10-22-49.zip)

### Bay of Biscay Anchovy dataset

This is a dataset for a hindcast for anchovy in the Bay of Biscay. ISIS-Fish V3.3.0.8.

* [Bay of Biscay Anchovy dataset](downloads/DataBaseAnchovyBoBHindcast_commented.zip)

### Bay of Biscay Pelagic Fishery dataset

This is a dataset for multi-species, multi-fleet simulation of the pelagic fishery in the Bay of Biscay.

* [Bay of Biscay Pelagic dataset](downloads/RegionGolfeGascognePelagique300710.zip)

### English Channel Flatfish Fishery dataset

This is a dataset for the flatfish fishery in the English Channel. ISIS-Fish V4.2.1.1

* [English Channel Flatfish dataset](downloads/Base_Channel_Flatfish.zip)

### Eastern English Channel mixed demersal Fishery dataset

This is the dataset for the mixed demersal fishery in the Eastern English
Channel, developped for the EU projects SOCIOEC and DISCARDLESS. V4.4.1.0.

* [Eastern English Channel mixed demersal Fishery dataset](downloads/DiscardLess_Channel_05102018.zip)

## ISIS-Fish (R version)

The R version of ISIS-Fish, FLISIS, was created by the FLR project and is not supported on this website, but you can
download it from here.

* [Download FLISIS](downloads/FLIsis_1.1.2.zip>) (dernière version : Janvier 2009)
* [FLR project website](https://flr-project.org/)
