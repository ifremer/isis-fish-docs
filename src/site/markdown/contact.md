# Contact/Help

ISIS-Fish is a development supported by Ifremer aimed at creating a user community. Facilities have been set up to
encourage communication between the members of the community.

## Mailing lists

Various mailing lists have been set up for specific purposes.

If you have a problem using ISIS-Fish we would like you to use the appropriate lists rather than send us personal
e-mails. Your question and the answer may be of interest to other people and, moreover, you may receive help from other
people on the list.

Even if French is the most common language used on those lists, you may write in English.

There is an archive of messages in the lists which you can search.

[List usage statistics](listStats.html)

### Lists for users

* isis-fish-users@list.isis-fish.org
  ([subscription page](http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-users)):
  mailing list for ISIS-Fish users.
* [User mailing-list archive](http://list.isis-fish.org/pipermail/isis-fish-users)
* isis-fish-data-commits@list.isis-fish.org
  ([subscription page](http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-data-commits)):
  mailing list receiving notifications of all modifications made to ISIS-Fish scripts source code. This email address
  does not accept emails.

### Lists for developers

* isis-fish-devel@list.isis-fish.org
  ([subscription page](http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-devel)):
  mailing list for ISIS-Fish developers.
* isis-fish-bugreport@list.isis-fish.org
  ([subscription page](http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-bugreport)):
  mailing list receiving notifications of all additions to the bug reports. This email address does not accept emails.
* isis-fish-commits@list.isis-fish.org
  ([subscription page](http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-commits)):
  mailing list receiving notifications of all modifications made to ISIS-Fish source code. This email address does not
  accept emails.

## Reporting tools

Those tools keep a record of bug reports and requests for improvements follow the progress.

Only account holders can submit requests to prevent spam. This subscription does not commit yourself to anything.
[Creating an account](https://forge.codelutin.com/account/register) is free and does not involve any obligations

Do not hesitate to tell us about any problem you might encounter and ask for any new or improved features that would
help you. This will help us to improve ISIS-Fish.

* [Issue tracking](https://forge.codelutin.com/projects/isis-fish/issues?set_filter=1&tracker_id=1): for any issues you have using ISIS-Fish.
* [Feature requests](https://forge.codelutin.com/projects/isis-fish/issues?set_filter=1&tracker_id=2): for any new or improved features that would help you.
* [Roadmap](https://forge.codelutin.com/projects/isis-fish/roadmap): shows the new features planned for upcoming versions and the progress.
