# The history of the ISIS-Fish project

## Prototype (1998-2000)

From 1998 to 2000, the prototype of the ISIS-Fish simulator was developed from the conceptual model drawn up by
Pelletier et al (2001)

## Versions 1.0 and 1.5 (2000 to 2003)

ISIS-Fish V1.0 was a generic toolbox for spatially explicit simulations to evaluate the effect of management measures on
the dynamics of complex fisheries (Version 1.0, Mahévas and Pelletier, 2004 and Version 1.5 Pelletier and Mahévas,
2005). Generic models were used for the simulations to make it suitable for different types of fisheries. The known
fisheries data were stored in an internal database and could be easily modified. The data included parameters for
characterizing each population, each fishing activity and each fishery management measure as well as the fleet dynamics.
The fishery exploitation model was spatially explicit and combined the dynamics of each population, each fleet and each
fishery management action. The model used a monthly timestep. The spatial and seasonal variations in capturability, the
seasonal migrations and the reproduction and recruitment processes were the main elements of the age-structured dynamic
population model.

Biological interactions are not modeled in ISIS-Fish as the aim of the model is to represent complex fisheries. The
fleet dynamics model represents the response of fishing fleets to the availability of the fish stocks and to the
management measures introduced to regulate the fishery.

Versions 1.0 and 1.5 allowed some flexibility in defining the model assumptions. For example the fishery dynamics could
be programmed using a script. The toolkit could, therefore, be used to compare the effects conventional management
measures, such as quotas and limits on fishing effort, and other measures, such as the definition of marine protected
areas.

Versions 1.0 and 1.5 did not include economic variables and it was not possible to define the fleet dynamics in terms of
economic conditions.

For further details, see the articles Mahévas and Pelletier (2004) and Pelletier and Mahévas (2005). The first of these
describes the various aspects of the model and the software development decisions for Version 1.0. The second deals with
the ability of Version 1.5 to evaluate the effects of marine protected areas in comparison with other fishery simulation
models.

These versions were based on Enterprise JavaBeans with a remote database, requiring a client-server configuration.

## Version 2.0 (2004 to 2005)

The second generation of ISIS-Fish was built on the earlier versions, adding economic variables and processes
determining fleet dynamics. As a result the components of the fleet model were redefined. The boats were characterized
by their technical specifications to take account of the transport costs and they were assigned a home port to calculate
the traveling time and the associated costs. Each boat was associated with a group of boats defined by a list of fishing
gears used, with parameters defining the fishing effort, the crews and associated costs, the fishing gear and boat
maintenance costs and other operating costs. A “strategy” represented a sub-group of boats with the same characteristics
(belonging to the same group of boats) and the same distribution of fishing effort between the different types of
fishing gear each month. Formulae were included for calculating the mortality per trip, per species, per class, per zone
and per gear as well as the costs and revenues.

The response of the fleets to fishery management measures, fluctuations in fish stocks and economic conditions may
affect the allocation of fishing effort. Most of the formulae controlling the fleet dynamics could be modified using a
script editor after selecting a predefined model or creating a new model. Many of the predefined models were gravity
models with the coefficients calculated from existing weight or value data for captures or landings.

Version 2.0 no longer used Enterprise JavaBeans and the database was integrated.

## Versions 3.0 to 3.3 (2006 to 2011)

The data storage was changed completely for Version 3. Each region had its own directory with an integrated database and
each simulation for each region had its own directory with a snapshot of the database used for the simulation, the
results of the simulation, the exported files, the scripts used for the simulation and the parameters for the
simulation. All the data and parameters used for each simulation were saved and, in principle, the simulation could be
re-run (not implemented).

The simulation engine was also upgraded to allow easier centralized monitoring. This upgrade was based on the analysis
of experimental designs in the MEXICO project.

A performance analysis of Version 2 had shown that the scripts executed 1000 to 10,000 times more slowly than Java. The
calculation speed was improved by writing all formulae and scripts in Java and compiling them on demand. Using Java also
improved the programming interface by providing facilities such as verification of the code entered and error reporting
with the line number in the code.

## Versions 4.0 (since 2011)

Version 4 did not introduce any major changes in technology but did introduce some changes to the model that made it
incompatible with previous versions. As users would need to modify the data for their region, the version number jumped
to 4.0.

Version 4 also introduced long awaited major changes in user scripts by renaming whole classes.

Overall, however, Version 4 is just an upgrade to version 3.