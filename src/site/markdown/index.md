<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# ISIS-Fish

ISIS-Fish is a complex fishery simulator. It is entirely written in Java.
In the second version, ECMAScript was used as the scripting language, since version 3,
Java is used directly. This provides syntax verification, color coding and identification
of lines with errors.

To use ISIS-Fish, Java must be installed on your computer. To use all ISIS-Fish features
R is also required.

## Characteristics

- Embedded database
- Creating as many fishing areas and populations as required
- Scripting language for creating rules
- Scripting language for modifying the simulation process
- Running multiple simulations, modifying one or more simulation parameters
- Running simulations on other computers (supercomputer for example),
- Spatial (on a map) or temporal (graph) presentation of results
- Publishing simulation results on a server
- Publishing results for a region on a server

## ISIS-Fish video

<!-- Video insérée par le script maven-isisfish.js -->

Download video : [ifremerengv2.mp4](http://data.isis-fish.org/isis-fish/movie/ifremerengv2.mp4)
(French version : [ifremerv7.mp4](http://data.isis-fish.org/isis-fish/movie/ifremerv7.mp4)).

## Development

This project was initiated and financed by Ifremer. The system was analyzed and created by:

- IRIN (Institut de Recherche en Informatique de Nantes - Nantes Computing Research Institute)
- Cogitec (http://www.cogitec.fr)
- Code Lutin (http://www.codelutin.com)
