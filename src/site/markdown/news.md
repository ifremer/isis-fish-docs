<!--
  #%L
  IsisFish
  %%
  Copyright (C) 1999 - 2022 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

# ISIS-Fish events

## 2020

- *July 21-22, 2020* - Java for ISIS-Fish workshop, Nantes, France
- *June 2020* – [MIMI](https://wwz.ifremer.fr/emh/content/download/142634/file/MIMI.pdf), Modèles, IMaginaires, et Incertitudes, Project kick-off
- *October 2020* – [SMAC](https://wwz.ifremer.fr/smac/), End-of-project meeting
- *January 23-24, 2020* – Complex modeling, modeling fishery dynamics and sensitivity analysis, M2 REA Agrocampus-Ouest, France

## 2019

- *June 26-38, 2019* – 14th French Fisheries Science Forum,  3 presentations of ISIS-Fish applications
- *June 2019* – [DEFIPEL](https://wwz.ifremer.fr/peche/Le-role-de-l-Ifremer/Recherche/Projets/Description-projets/DEFIPEL), DEveloppement d’une approche de gestion intégrée de la Filière petits PELagiques: Project kick-off
- *September 2019* – [MACCO](https://wwz.ifremer.fr/emh/content/download/141679/file/MACCO.pdf), Mesures de gestion et espèces ACCessOires dans les pêcheries démersales du golfe de Gascogne: Project kick-off
- *December 2019* – [PECHALO](https://archimer.ifremer.fr/doc/00622/73377/), Project deliverables - report available at https://archimer.ifremer.fr/doc/00622/73377/ & Shiny app at https://pechalo-results.isis-fish.org/Scripts/
- *January 23-24, 2019* – Complex modeling, modeling fishery dynamics and sensitivity analysis, M2 REA Agrocampus-Ouest, France

## 2018

- *November 2018* – [DiscardLess](http://www.discardless.eu/deliverables/entry/final-ecosystem-scale-models-results-of-scenarios-baseline-alternative-mana), - Project deliverables : Final fishery scale models and results of scenarios (baseline, alternative management scenarios and DMS scenarios), report available at http://www.discardless.eu/deliverables/entry/final-fishery-scale-models-and-results-of-scenarios-baseline-alternative-ma
- *December 2018* – [GALION](http://www.amop.fr/le-projet-galion/), - Project deliverables - report available at https://archimer.ifremer.fr/recordview
- *January 23-24, 2018* – Complex modeling, modeling fishery dynamics and sensitivity analysis, M2 REA Agrocampus-Ouest, France

## 2017

- *June 28-30, 2017* – **13th French Fisheries Science Forum**,  3 presentations of ISIS-Fish applications and participation in the Individual-Based Modeling Workshop
- *May 2017* – **ISIS-Fish animated film release**
- *May 4, 2017* – **COSELMAR forecasting workshop**, combining qualitative and quantitative methods for constructing
  socio-ecosystem management scenarios in the Bay of Biscay, Nantes, France
- *February 7, 2017* – **Presentation of the Hyblab Data Journalism project**, “It’s not man that takes from the sea, it’s the sea that takes from man” – student project run by the “COSELMAR scouts”
- *January 23-24, 2017* – **Complex modeling, modeling fishery dynamics and sensitivity analysis, M2 REA Agrocampus-Ouest, France**

## 2016

- *December 5, 2016* – **ISIS-Fish open day for users**, Video room 2, Nantes, France
- *September 15, 2016* – **Final report on the APPELS project**, Nantes, France
- *June 2016* – **MSEAS Conference**, Brest, France

  - Paper by S. Lehuta: Indicator decision tree for managing fisheries
  - Paper by S. Lehuta, Marie Savina-Rolland,Pierre Bourdaud, Raphaël Girardin, Morgane Travers-Trolet, Youen Vermard
    and Zoé Provot: Lessons from a multi-model participatory approach for the evaluation of discard reduction scenarios
    in the Eastern English Channel

- *February 2016* – **DISCARDLESS project** meetings. ISIS-Fish is used for evaluation discard reduction strategies in
  collaboration with the demersal fishery industry
- *January 25-26, 2016* – **Complex modeling, modeling fishery dynamics and sensitivity analysis – M2 REA Agrocampus-Ouest, France**
- *January 22, 2016* – **ISIS-Fish open day**, Room 11, Nantes, France
- *January 2016* - Project start-up GALION_ - Utiliser la connaissance de la distribution des captures des pêcheries du golfe du Lion pour limiter les rejets des espèces sous taille et permettre une exploitation durable.

## 2015

- *March 13, 2015* – **Oceanographic intensive computing unit (Pole de Calcul Intensif pour la Mer) open day**, [PCIM](http://w3z.ifremer.fr/espacecommunication/Planete-Ifremer/En-bref/Journee-PCIM), Brest, France
- *January 29-30, 2015* – **Complex modeling, modeling fishery dynamics and sensitivity analysis – M2 REA Agrocampus-Ouest, France**

<img src="images/Agro.JPG"
  alt="Agro"
  style="height: 290px; width: 389px;" />

## 2014

- *December 15-16, 2014* – **ISIS-Fish open day for users and modeling day", Nantes, France**
- *November 4-6, 2014* – **Optimization workshop** in conjunction with the MEXICO network – La Rochelle, France
- *September 15-19, 2014* – **ICES annual conference 2014**

  - Paper by S. Lehuta: Investigating spatial and demographic indicator-based Harvest Control Rules for the Eastern English Channel mixed fisheries
  - Paper by Y. Reecht: Taking into account medium term impact of conservation measures on mixed fisheries across the MPA network in the Eastern English Channel for conservation planning

- *January 17, 2014* – **Oceanographic intensive computing unit (Pole de Calcul Intensif pour la Mer) open day**,
  `PCIM`_, Brest, France
  - Presentation by Sigrid Lehuta, Stéphanie Mahévas, Benjamin Poussin and Loïc Gasche on the use of the Caparmor supercomputer for ISIS-Fish


## 2013

- *December 5-6, 2013* – **ISIS-Fish seminar for users – Nantes, France**
- *November 26, 2013* – **Big data day - Brest, France**, [BigData](http://wwz.ifremer.fr/bigdata)
- *October 28-31, 2013* – **ICES SGIMM Meeting**, [ICES SGIMM Meeting 2013](http://www.ices.dk/community/groups/Pages/SGIMM.aspx)
- *September 23-27, 2013* – **ICES annual conference 2013**

  - Paper by S. Mahévas: Modelling fishermen behavior is firstly a question of spatial and temporal scale
  - Paper by S. Lehuta: Toward new Indices for Harvest Control Rules in the Eastern Channel fisheries

- *June 17-21, 2013* – **AFH Forum**, Bordeaux, France, [Fisheries Science Forum 2013](http://halieutique.agrocampus-ouest.fr/afh/index.html)
- *March 2013* – Publication of [Analyse de sensibilité et exploration de modèles](http://www.quae.com/fr/r2142-analyse-de-sensibilite-et-exploration-de-modeles.html) (Sensitivity analysis and model
  exploration) in conjunction with the MEXICO network
- *January 2013* –  **EU VECTORS project meeting** – Integrated Modeling

## 2012

- *December 6-7, 2012* – **ISIS-Fish seminar for users – Nantes, France**
- *November 22-23, 2012* – *[Mexico](http://reseau-mexico.fr/welcome) meetings* - Nantes, FRANCE ,  MEXICO_
- *September 17-21, 2012* – [ICES annual conference 2012](http://www.ices.dk/asc2012/) , Bergen, Norway
- *June 4-8, 2012* – Sensitivity analysis, MEXICO training course [Ecole Chercheur 2012](http://reseau-mexico.fr/ecoleChercheursMexico2012),  ISIS-Fish workshop, Lyon, France
- *April 23-26, 2012* – Initial meeting for the EU [MYFISH](http://ec.europa.eu/research/bioeconomy/agriculture/projects/myfish_en.htm) project, Vigo, Spain
- *January 19-20, 2012* – [Mexico](http://reseau-mexico.fr/welcome) meeting

## 2011

- *December 5-6, 2011* – ISIS-Fish seminar for users – Nantes, France
- *June 27, 2011* – Sensitivity analysis workshop [Fisheries science forum 2011](http://halieutique.agrocampus-ouest.fr/afh/index.html) – Boulogne sur Mer, France
- *June 6-8, 2011* – [Mexico](http://reseau-mexico.fr/welcome) internal meeting
- *April 5, 2011* – ISIS-Fish 3.3.0.8 released

## 2010

- *September 20-24, 2010* – [ICES annual conference 2010](http://www.ices.dk/iceswork/asc/2010/index.asp) – Nantes, France
- *July 7, 2010* – ISIS-Fish conference at [RMLL](http://rmll.info) – Bordeaux, France
- *June 7-11, 2010* – Sensitivity analysis, MEXICO training course [Ecole chercheur](http://www.reseau-mexico.fr/node/165) – Giens, France

## 2009

- *December 1-3, 2009* – IFREMER / DFO Canada workshop on Ecosystem approaches – Rimouski, Canada
- *September 21-26, 2009* – [ICES annual conference 2009](http://www.ices.dk/iceswork/asc/2009/index.asp) – Berlin, Germany
- *September 14-18, 2009* – ISIS-Fish seminar for users – Nantes, France
- *September 8-11, 2009* – [Franco-Australian workshop on bio-economic models](http://www.umr-amure.fr/pg_workshop_fast.php)
- *August 29 to September 4, 2009* – [ICES Benchmark Workshop on Short-lived Species](http://www.ices.dk/workinggroups/ViewWorkingGroup.aspx?ID=345) (WKSHORT) – comparison of models for anchovy, Bergen, Norway
- *May 11-14, 2009* – Sensitivity analysis, MEXICO training course École chercheur – Giens, France

## 2008

- *December 3-4, 2008* – [Mexico](http://reseau-mexico.fr/welcome) meeting, Montpellier, France
- *November 28, 2008* – brainstorming: ISIS-Fish model for anchovy in the Bay of Biscay – Ecology and Fishery Modeling Department, IFREMER, Nantes, France
- *September 22-26, 2008* – [ICES annual conference 2008](http://www.ices.dk/iceswork/asc/2008/index.asp) – Halifax, Canada
- *July 2, 2008* – Presentation of ISIS-Fish at [RMLL](http://rmll.info)
- *June 5-6, 2008* – [Mexico](http://reseau-mexico.fr/welcome) meeting, Nantes, France
