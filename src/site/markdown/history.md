# Versions

The ISIS-Fish simulator has undergone major changes throughout its development which are summarized in the [project history](history/history.html). The [change log](https://forge.codelutin.com/projects/isis-fish/roadmap?completed=1) for the current version is accessible in from an external website.

Previous versions
-----------------

Overviews of the previous versions of the ISIS-Fish simulator are available on this website.

- [Version 1](history/v1.html)
- [Version 2](history/v2.html)
- [Version 3](history/v3.html)
